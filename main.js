function getStacktrace() {
    try {
        // noinspection JSUnresolvedFunction
        _CORE_throwException();
    } catch(e) {
        let lines = e.stack.split('\n');
        let trace = [];

        for(let i = lines.length > 3 ? 2 : 1; i < lines.length; i++) {
            let line = lines[i].substr(lines[i].indexOf(':')+1).trim();
            let split = line.split(':');
            if(split.length >  1) {
                let lineSplit = split[1].indexOf(' ');
                trace.push({file: split[0], line: split[1].substr(0, lineSplit>0?lineSplit:split[1].length)});
            }
        }
        return trace;
    }
}
let window = eval('this;');
Object.defineProperty(window, '__FILE__', {
    get: function() {
        let stacktrace = getStacktrace();
        return stacktrace[0].file;
    },
    configurable: true
});
Object.defineProperty(window, '__DIR__', {
    get: function() {
        let stacktrace = getStacktrace();
        let dirname=function(a){let b=a.split("/"),c=[];return(b.forEach(function(a){return""===a||"."===a?void 0:".."===a?void c.pop():void c.push(a)}),1===c.length)?c[0]:(c.pop(),c.join("/")+"/")};
        return dirname(stacktrace[0].file);
    },
    configurable: true
});
var App = {};
App.chatCommands = {};
KnuddelsServer.execute(__DIR__+'/includes/init.js');
