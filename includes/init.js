if(typeof K3AF === 'undefined') {
    var K3AF = function () {
    };
}
K3AF.VERSION = "0.0.0.1";

K3AF.debug = function(msg) {
    if(!KnuddelsServer.isTestSystem()) return;
    KnuddelsServer.getDefaultLogger().debug('K3AF: ' + msg);
};
K3AF.error = function(msg) {
    KnuddelsServer.getDefaultLogger().error('K3AF: ' + msg);
};
require(__DIR__ + '/classes/FileSystem.js');
K3AF.BASEPATH = K3AF.FileSystem.dirname(__DIR__);
K3AF.debug('BASEPATH: ' + K3AF.BASEPATH);
K3AF.FileSystem.executeDir(__DIR__ + '/extensions/', true);
K3AF.FileSystem.executeDir(__DIR__ + '/classes/', true);
K3AF.FileSystem.execute(__DIR__+'/module/ModuleManager.js');
K3AF.FileSystem.executeDir(__DIR__+'/module/');


ModuleManager.self.refreshHooks(false, false);



