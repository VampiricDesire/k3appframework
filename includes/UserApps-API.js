/**
 * @see https://developer.knuddels.de/docs/classes/App.html
 * @class App
 */
function App() {}

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_mayJoinChannel
 * @param {User} user
 * @return {ChannelJoinPermission} 
 */
App.prototype.mayJoinChannel = function(user) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_mayShowPublicMessage
 * @param {PublicMessage} publicMessage
 * @return {Boolean} true, wenn die Nachricht angezeigt werden soll, false im anderen Fall.
 */
App.prototype.mayShowPublicMessage = function(publicMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_mayShowPublicActionMessage
 * @param {PublicActionMessage} publicActionMessage
 * @return {Boolean} true, wenn die Handlung ausgeführt werden soll, false im anderen Fall.
 */
App.prototype.mayShowPublicActionMessage = function(publicActionMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onAppStart
 * @return {void} 
 */
App.prototype.onAppStart = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onBeforeKnuddelReceived
 * @param {KnuddelTransfer} knuddelTransfer
 * @return {void} 
 */
App.prototype.onBeforeKnuddelReceived = function(knuddelTransfer) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onKnuddelReceived
 * @param {User} sender
 * @param {BotUser} receiver
 * @param {KnuddelAmount} knuddelAmount
 * @param {String} transferReason
 * @return {void} 
 */
App.prototype.onKnuddelReceived = function(sender, receiver, knuddelAmount, transferReason) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onPrepareShutdown
 * @param {Number} secondsTillShutdown
 * @return {void} 
 */
App.prototype.onPrepareShutdown = function(secondsTillShutdown) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onPrivateMessage
 * @param {PrivateMessage} privateMessage
 * @return {void} 
 */
App.prototype.onPrivateMessage = function(privateMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onPublicMessage
 * @param {PublicMessage} publicMessage
 * @return {void} 
 */
App.prototype.onPublicMessage = function(publicMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onPublicEventMessage
 * @param {PublicEventMessage} publicEventMessage
 * @return {void} 
 */
App.prototype.onPublicEventMessage = function(publicEventMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onPublicActionMessage
 * @param {PublicActionMessage} publicActionMessage
 * @return {void} 
 */
App.prototype.onPublicActionMessage = function(publicActionMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onShutdown
 * @return {void} 
 */
App.prototype.onShutdown = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onUserDiced
 * @param {DiceEvent} diceEvent
 * @return {void} 
 */
App.prototype.onUserDiced = function(diceEvent) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onUserJoined
 * @param {User} user
 * @return {void} 
 */
App.prototype.onUserJoined = function(user) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onUserLeft
 * @param {User} user
 * @return {void} 
 */
App.prototype.onUserLeft = function(user) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onAppEventReceived
 * @param {AppInstance} appInstance
 * @param {String} type
 * @param {Object} data
 * @return {void} 
 */
App.prototype.onAppEventReceived = function(appInstance, type, data) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onEventReceived
 * @param {User} user
 * @param {String} type
 * @param {Object} data
 * @param {AppContentSession} appContentSession
 * @return {void} 
 */
App.prototype.onEventReceived = function(user, type, data, appContentSession) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onAccountReceivedKnuddel
 * @param {User} sender
 * @param {BotUser} receiver
 * @param {KnuddelAmount} knuddelAmount
 * @param {String} transferReason
 * @param {KnuddelAccount} knuddelAccount
 * @return {void} 
 */
App.prototype.onAccountReceivedKnuddel = function(sender, receiver, knuddelAmount, transferReason, knuddelAccount) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onAccountChangedKnuddelAmount
 * @param {User} user
 * @param {KnuddelAccount} knuddelAccount
 * @param {KnuddelAmount} oldKnuddelAmount
 * @param {KnuddelAmount} newKnuddelAmount
 * @return {void} 
 */
App.prototype.onAccountChangedKnuddelAmount = function(user, knuddelAccount, oldKnuddelAmount, newKnuddelAmount) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onUserDeleted
 * @param {Number} userId
 * @param {UserPersistence} userPersistence
 * @return {void} 
 */
App.prototype.onUserDeleted = function(userId, userPersistence) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_onDeveloperCommand
 * @since AppServer 108662, ChatServer 108662
 * @param {User} user
 * @param {String} params
 * @return {void} 
 */
App.prototype.onDeveloperCommand = function(user, params) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#method_mayUserDice
 * @param {User} user
 * @param {DiceConfiguration} diceConfig
 * @return {Boolean} true, wenn der User würfeln darf, false im anderen Fall.
 */
App.prototype.mayUserDice = function(user, diceConfig) {};

/**
 * @see https://developer.knuddels.de/docs/classes/App.html#property_chatCommands
 * Ermöglicht das Registrieren eigener Chatbefehle.
 * In einem Channel kann nur eine App laufen, die einen bestimmten Chatbefehl nutzt.
 * Versucht eine zweite App einen Chatbefehl zu registrieren, den eine andere
 * App bereits nutzt, so wird ein Fehler geloggt und die App startet nicht bzw. fährt herunter.
 * 
 * Die Struktur eines registrierten Chatbefehls ist:
 * commandName: function (user, params, command) {}
 * 
 * 
 * 
 * commandName ist der Name der Funktion, wie sie aufgerufen wird (beispielsweise /commandname)
 * user ist der Nutzer, der die Funktion aufgerufen hat
 * params sind die Parameter, die der Nutzer hinter dem Befehl eingegeben hat (beispielsweise /commandname params)
 * command ist der Name des Befehl selbst (beispielsweise commandName)
 * 

* @type {Object}
*/
App.chatCommands = 'chatCommands';



/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html
 * @class Toplist
 */
function Toplist() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_getUserPersistenceNumberKey
 * @return {String} userPersistenceNumberKey mit dem die Topliste erzeugt wurde
 */
Toplist.prototype.getUserPersistenceNumberKey = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_getDisplayName
 * @return {String} Anzeigename der Topliste
 */
Toplist.prototype.getDisplayName = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_getChatCommand
 * @param {User|Number} [user|userId]
 * @return {String} 
 */
Toplist.prototype.getChatCommand = function(userOruserId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_getLabel
 * @param {User|Number} user|userId
 * @return {String} Anzeigename
 */
Toplist.prototype.getLabel = function(userOruserId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_addLabelChangeListener
 * @param {Function} listener
 * @return {void} 
 */
Toplist.prototype.addLabelChangeListener = function(listener) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_removeLabelChangeListener
 * @param {Function} listener
 * @return {void} 
 */
Toplist.prototype.removeLabelChangeListener = function(listener) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_addRankChangeListener
 * @param {Function} listener
 * @return {void} 
 */
Toplist.prototype.addRankChangeListener = function(listener) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Toplist.html#method_removeRankChangeListener
 * @param {Function} listener
 * @return {void} 
 */
Toplist.prototype.removeRankChangeListener = function(listener) {};




/**
 * @see https://developer.knuddels.de/docs/classes/SingleDiceResult.html
 * @class SingleDiceResult
 */
function SingleDiceResult() {}

/**
 * @see https://developer.knuddels.de/docs/classes/SingleDiceResult.html#method_getDice
 * @return {Dice} 
 */
SingleDiceResult.prototype.getDice = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/SingleDiceResult.html#method_valuesRolled
 * @return {Number[]} 
 */
SingleDiceResult.prototype.valuesRolled = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/SingleDiceResult.html#method_sum
 * @return {Number} 
 */
SingleDiceResult.prototype.sum = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChatServerInfo.html
 * @class ChatServerInfo
 * @extends ServerInfo
 */
function ChatServerInfo() {}
ChatServerInfo.prototype = new ServerInfo;

/**
 * @see https://developer.knuddels.de/docs/classes/ChatServerInfo.html#method_isTestSystem
 * @return {Boolean} 
 */
ChatServerInfo.prototype.isTestSystem = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppContentSession.html
 * @class AppContentSession
 */
function AppContentSession() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppContentSession.html#method_sendEvent
 * @param {String} type
 * @param {Object} [data]
 * @return {void} 
 */
AppContentSession.prototype.sendEvent = function(type, data) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContentSession.html#method_getAppViewMode
 * @return {AppViewMode} 
 */
AppContentSession.prototype.getAppViewMode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContentSession.html#method_remove
 * @return {void} 
 */
AppContentSession.prototype.remove = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContentSession.html#method_getUser
 * @return {User} user
 */
AppContentSession.prototype.getUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContentSession.html#method_getAppContent
 * @return {AppContent} 
 */
AppContentSession.prototype.getAppContent = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/String.html
 * @class String
 */
function String() {}

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_escapeKCode
 * @return {String} KCode escaped
 */
String.prototype.escapeKCode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_stripKCode
 * @return {String} 
 */
String.prototype.stripKCode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_startsWith
 * @param {String} prefix
 * @return {Boolean} true, wenn der String mit prefix beginnt.
 */
String.prototype.startsWith = function(prefix) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_endsWith
 * @param {String} suffix
 * @return {Boolean} true, wenn der String mit suffix endet.
 */
String.prototype.endsWith = function(suffix) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_getPixelWidth
 * @param {Number} fontSize
 * @param {Boolean} isBold
 * @return {Number} 
 */
String.prototype.getPixelWidth = function(fontSize, isBold) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_limitString
 * @param {Number} fontSize
 * @param {Boolean} isBold
 * @param {Number} maxPixelWidth
 * @param {String} [abbreviationMarker]
 * @return {String} 
 */
String.prototype.limitString = function(fontSize, isBold, maxPixelWidth, abbreviationMarker) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_contains
 * @param {String} needle
 * @return {Boolean} 
 */
String.prototype.contains = function(needle) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_minimalConversionCost
 * @since AppServer 82271
 * @param {String} otherString
 * @return {Number} 
 */
String.prototype.minimalConversionCost = function(otherString) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_hasOnlyNicknameCharacters
 * @since AppServer 82271
 * @return {Boolean} 
 */
String.prototype.hasOnlyNicknameCharacters = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_hasOnlyDigits
 * @since AppServer 82271
 * @return {Boolean} 
 */
String.prototype.hasOnlyDigits = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_hasOnlyAlphanumericalAndWhitespaceCharacters
 * @since AppServer 82271
 * @return {Boolean} 
 */
String.prototype.hasOnlyAlphanumericalAndWhitespaceCharacters = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_isEmpty
 * @since AppServer 92695
 * @return {Boolean} 
 */
String.prototype.isEmpty = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_toCamelCase
 * @since AppServer 92695
 * @return {String} 
 */
String.prototype.toCamelCase = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_capitalize
 * @since AppServer 92695
 * @return {String} 
 */
String.prototype.capitalize = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_replaceAll
 * @param {String} search
 * @param {String} replacement
 * @return {String} 
 */
String.prototype.replaceAll = function(search, replacement) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_replaceAll
 * @param {RegExp} regexp
 * @param {String} replacement
 * @return {String} 
 */
String.prototype.replaceAll = function(regexp, replacement) {};

/**
 * @see https://developer.knuddels.de/docs/classes/String.html#method_isOk
 * @since ChatServer 82262, AppServer 82262
 * @return {Boolean} 
 */
String.prototype.isOk = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/Gender.html
 * @class Gender
 */
function Gender() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Gender.html#property_Male
 * Das Geschlecht ist männlich.

* @type {Gender}
*/
Gender.Male = 'Male';
/**
 * @see https://developer.knuddels.de/docs/classes/Gender.html#property_Female
 * Das Geschlecht ist weiblich.

* @type {Gender}
*/
Gender.Female = 'Female';
/**
 * @see https://developer.knuddels.de/docs/classes/Gender.html#property_Unknown
 * Das Geschlecht ist nicht bekannt.

* @type {Gender}
*/
Gender.Unknown = 'Unknown';



/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html
 * @class UserStatus
 */
function UserStatus() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#method_getNumericStatus
 * @return {Number} 
 */
UserStatus.prototype.getNumericStatus = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#method_isAtLeast
 * @param {UserStatus} otherUserStatus
 * @return {Boolean} 
 */
UserStatus.prototype.isAtLeast = function(otherUserStatus) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_Newbie
 * 

* @type {UserStatus}
*/
UserStatus.Newbie = 'Newbie';
/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_Family
 * 

* @type {UserStatus}
*/
UserStatus.Family = 'Family';
/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_Stammi
 * 

* @type {UserStatus}
*/
UserStatus.Stammi = 'Stammi';
/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_HonoryMember
 * 

* @type {UserStatus}
*/
UserStatus.HonoryMember = 'HonoryMember';
/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_Admin
 * 

* @type {UserStatus}
*/
UserStatus.Admin = 'Admin';
/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_SystemBot
 * 

* @type {UserStatus}
*/
UserStatus.SystemBot = 'SystemBot';
/**
 * @see https://developer.knuddels.de/docs/classes/UserStatus.html#property_Sysadmin
 * 

* @type {UserStatus}
*/
UserStatus.Sysadmin = 'Sysadmin';



/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html
 * @class Client.Color
 */
Client.Color = function(){}

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html#method_fromRGB
 * @static
 * @param {Number} red
 * @param {Number} green
 * @param {Number} blue
 * @return {Color} 
 */
Client.Color.fromRGB = function(red, green, blue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html#method_fromHexString
 * @static
 * @param {String} colorString
 * @return {Color} 
 */
Client.Color.fromHexString = function(colorString) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html#method_getRed
 * @return {Number} 
 */
Client.Color.prototype.getRed = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html#method_getGreen
 * @return {Number} 
 */
Client.Color.prototype.getGreen = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html#method_getBlue
 * @return {Number} 
 */
Client.Color.prototype.getBlue = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Color.html#method_asHexString
 * @return {String} 
 */
Client.Color.prototype.asHexString = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/AuthenticityClassification.html
 * @class AuthenticityClassification
 */
function AuthenticityClassification() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AuthenticityClassification.html#method_getDisplayText
 * @since AppServer 94663, ChatServer 94663
 * @return {String} 
 */
AuthenticityClassification.prototype.getDisplayText = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AuthenticityClassification.html#property_ServiceNotAvailable
 * Der Service zur Erkennung der Echtheit von Nutzern ist derzeit für die API nicht erreichbar.

* @type {AuthenticityClassification}
*/
AuthenticityClassification.ServiceNotAvailable = 'ServiceNotAvailable';
/**
 * @see https://developer.knuddels.de/docs/classes/AuthenticityClassification.html#property_Unknown
 * Es liegen zu wenige Informationen vor, um die Echtheit des Nutzers zu bestätigen.

* @type {AuthenticityClassification}
*/
AuthenticityClassification.Unknown = 'Unknown';
/**
 * @see https://developer.knuddels.de/docs/classes/AuthenticityClassification.html#property_Trusted
 * Der Nutzer ist sehr wahrscheinlich echt.

* @type {AuthenticityClassification}
*/
AuthenticityClassification.Trusted = 'Trusted';
/**
 * @see https://developer.knuddels.de/docs/classes/AuthenticityClassification.html#property_VeryTrusted
 * Der Nutzer wurde als echt erkannt.

* @type {AuthenticityClassification}
*/
AuthenticityClassification.VeryTrusted = 'VeryTrusted';



/**
 * @see https://developer.knuddels.de/docs/classes/Client.Event.html
 * @class Client.Event
 */
Client.Event = function(){}

/**
 * @see https://developer.knuddels.de/docs/classes/Client.Event.html#method_Event
 * @static
 * @param {String} type
 * @param {Object} data
 * @return {void} 
 */
Client.Event.Event = function(type, data) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html
 * @class ToplistLabelChangeEvent
 */
function ToplistLabelChangeEvent() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html#method_getToplist
 * @return {Toplist} 
 */
ToplistLabelChangeEvent.prototype.getToplist = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html#method_getOldLabel
 * @return {String} 
 */
ToplistLabelChangeEvent.prototype.getOldLabel = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html#method_getNewLabel
 * @return {String} 
 */
ToplistLabelChangeEvent.prototype.getNewLabel = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html#method_getUser
 * @return {User} 
 */
ToplistLabelChangeEvent.prototype.getUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html#method_getOldValue
 * @return {Number} 
 */
ToplistLabelChangeEvent.prototype.getOldValue = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistLabelChangeEvent.html#method_getNewValue
 * @return {Number} 
 */
ToplistLabelChangeEvent.prototype.getNewValue = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/PrivateMessage.html
 * @class PrivateMessage
 * @extends Message
 */
function PrivateMessage() {}
PrivateMessage.prototype = new Message;

/**
 * @see https://developer.knuddels.de/docs/classes/PrivateMessage.html#method_getReceivingUsers
 * @return {User[]} 
 */
PrivateMessage.prototype.getReceivingUsers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/PrivateMessage.html#method_sendReply
 * @param {String} text
 * @return {void} 
 */
PrivateMessage.prototype.sendReply = function(text) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ToplistDisplayType.html
 * @class ToplistDisplayType
 */
function ToplistDisplayType() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistDisplayType.html#property_Label
 * Nur Anzeigename anzeigen.

* @type {ToplistDisplayType}
*/
ToplistDisplayType.Label = 'Label';
/**
 * @see https://developer.knuddels.de/docs/classes/ToplistDisplayType.html#property_Value
 * Gespeicherten Wert anzeigen.

* @type {ToplistDisplayType}
*/
ToplistDisplayType.Value = 'Value';
/**
 * @see https://developer.knuddels.de/docs/classes/ToplistDisplayType.html#property_LabelAndRank
 * Anzeigename und Rang anzeigen.

* @type {ToplistDisplayType}
*/
ToplistDisplayType.LabelAndRank = 'LabelAndRank';
/**
 * @see https://developer.knuddels.de/docs/classes/ToplistDisplayType.html#property_ValueAndRank
 * Wert und Rang anzeigen.

* @type {ToplistDisplayType}
*/
ToplistDisplayType.ValueAndRank = 'ValueAndRank';



/**
 * @see https://developer.knuddels.de/docs/classes/Client.html
 * @class Client
 */
function Client() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_close
 * @static
 * @return {void} 
 */
Client.close = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_sendEvent
 * @static
 * @param {String} type
 * @param {Object} data
 * @return {void} 
 */
Client.sendEvent = function(type, data) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_executeSlashCommand
 * @static
 * @param {String} command
 * @return {void} 
 */
Client.executeSlashCommand = function(command) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_includeJS
 * @static
 * @param {String} files
 * @return {void} 
 */
Client.includeJS = function(files) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_addEventListener
 * @static
 * @param {String} type
 * @param {Function} callback
 * @return {void} 
 */
Client.addEventListener = function(type, callback) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_dispatchEvent
 * @static
 * @param {Client.Event} event
 * @return {void} 
 */
Client.dispatchEvent = function(event) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_removeEventListener
 * @static
 * @param {String} type
 * @return {void} 
 */
Client.removeEventListener = function(type) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_removeEventListener
 * @static
 * @param {String} type
 * @param {Function} callback
 * @return {void} 
 */
Client.removeEventListener = function(type, callback) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_includeCSS
 * @static
 * @param {String} files
 * @return {void} 
 */
Client.includeCSS = function(files) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_playSound
 * @static
 * @param {String} fileName
 * @return {void} 
 */
Client.playSound = function(fileName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_prefetchSound
 * @static
 * @param {String} fileName
 * @return {void} 
 */
Client.prefetchSound = function(fileName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_freeSound
 * @static
 * @param {String} fileName
 * @return {void} 
 */
Client.freeSound = function(fileName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_getHostFrame
 * @static
 * @return {Client.HostFrame} 
 */
Client.getHostFrame = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_getNick
 * @static
 * @return {String} Nickname des Betrachters
 */
Client.getNick = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_getClientType
 * @static
 * @return {ClientType} 
 */
Client.getClientType = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_getCacheInvalidationId
 * @static
 * @return {String} 
 */
Client.getCacheInvalidationId = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_getDirectConnection
 * @static
 * @return {Promise} 
 */
Client.getDirectConnection = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_addConnectionTypeChangeListener
 * @static
 * @param {Function} callback
 * @return {void} 
 */
Client.addConnectionTypeChangeListener = function(callback) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#method_removeConnectionTypeChangeListener
 * @static
 * @param {Function} callback
 * @return {void} 
 */
Client.removeConnectionTypeChangeListener = function(callback) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.html#property_pageData
 * Beinhaltet die JSON-Daten, die beim Erstellen des HTMLFile übergeben wurden.

* @type {Json}
*/
Client.pageData = 'pageData';



/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html
 * @class Client.HostFrame
 */
Client.HostFrame = function(){}

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_setTitle
 * @param {String} newTitle
 * @return {void} 
 */
Client.HostFrame.prototype.setTitle = function(newTitle) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_setBackgroundColor
 * @param {Color} newColor
 * @param {Number} [durationMillis]
 * @return {void} 
 */
Client.HostFrame.prototype.setBackgroundColor = function(newColor, durationMillis) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_setIcons
 * @since Applet: 9.0bwj, AppServer: 84904
 * @param {String} path
 * @return {void} 
 */
Client.HostFrame.prototype.setIcons = function(path) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_setResizable
 * @param {Boolean} resizable
 * @return {void} 
 */
Client.HostFrame.prototype.setResizable = function(resizable) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_focus
 * @since Applet: 9.0bwj, AppServer: 84904
 * @return {void} 
 */
Client.HostFrame.prototype.focus = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_setSize
 * @since Applet: 9.0bwj, AppServer: 84516
 * @param {Number} width
 * @param {Number} height
 * @return {void} 
 */
Client.HostFrame.prototype.setSize = function(width, height) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_getAppViewMode
 * @since Applet: 9.0byl
 * @return {String} 
 */
Client.HostFrame.prototype.getAppViewMode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Client.HostFrame.html#method_getBrowserType
 * @since Applet: 9.0bzp
 * @return {String} 
 */
Client.HostFrame.prototype.getBrowserType = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPotState.html
 * @class KnuddelPotState
 */
function KnuddelPotState() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPotState.html#property_Open
 * Der KnuddelPot ist geöffnet und kann neue Teilnehmer annehmen.

* @type {KnuddelPotState}
*/
KnuddelPotState.Open = 'Open';
/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPotState.html#property_Sealed
 * Der KnuddelPot ist versiegelt und kann keine neuen Teilnehmer annehmen.

* @type {KnuddelPotState}
*/
KnuddelPotState.Sealed = 'Sealed';
/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPotState.html#property_Closed
 * Der KnuddelPot ist beendet und die Knuddel bereits ausgezahlt.

* @type {KnuddelPotState}
*/
KnuddelPotState.Closed = 'Closed';



/**
 * @see https://developer.knuddels.de/docs/classes/RootAppInstance.html
 * @class RootAppInstance
 * @extends AppInstance
 */
function RootAppInstance() {}
RootAppInstance.prototype = new AppInstance;

/**
 * @see https://developer.knuddels.de/docs/classes/RootAppInstance.html#method_updateApp
 * @param {String} [message]
 * @param {String} [logMessage]
 * @return {Number} Anzahl Sekunden bis das Update planmäßig durchgeführt wird.
 */
RootAppInstance.prototype.updateApp = function(message, logMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/RootAppInstance.html#method_cancelUpdateApp
 * @since AppServer 98117
 * @return {Boolean} true falls ein Update abgebrochen wurde.
 */
RootAppInstance.prototype.cancelUpdateApp = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/RootAppInstance.html#method_stopApp
 * @param {String} [message]
 * @param {String} [logMessage]
 * @return {void} 
 */
RootAppInstance.prototype.stopApp = function(message, logMessage) {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html
 * @class AppInstance
 */
function AppInstance() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_getAppInfo
 * @return {AppInfo} 
 */
AppInstance.prototype.getAppInfo = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_sendAppEvent
 * @param {String} type
 * @param {Object} data
 * @return {void} 
 */
AppInstance.prototype.sendAppEvent = function(type, data) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_isRootInstance
 * @return {Boolean} 
 */
AppInstance.prototype.isRootInstance = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_getRootInstance
 * @return {RootAppInstance} 
 */
AppInstance.prototype.getRootInstance = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_getAllInstances
 * @param {Boolean} [includeSelf]
 * @return {AppInstance[]} 
 */
AppInstance.prototype.getAllInstances = function(includeSelf) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_getStartDate
 * @return {Date} 
 */
AppInstance.prototype.getStartDate = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_getRegisteredChatCommandNames
 * @return {String[]|null} 
 */
AppInstance.prototype.getRegisteredChatCommandNames = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInstance.html#method_getChannelName
 * @return {String} 
 */
AppInstance.prototype.getChannelName = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelInformation.html
 * @class ChannelInformation
 */
function ChannelInformation() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelInformation.html#method_getTopic
 * @return {String} 
 */
ChannelInformation.prototype.getTopic = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelInformation.html#method_setTopic
 * @param {String} topic
 * @param {Boolean} showMessage
 * @return {void} 
 */
ChannelInformation.prototype.setTopic = function(topic, showMessage) {};




/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html
 * @class UserAccess
 */
function UserAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_getUserId
 * @param {String} nick
 * @return {Number} userId des Users mit dem angegebenen Nick
 */
UserAccess.prototype.getUserId = function(nick) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_exists
 * @param {String} nick
 * @return {Boolean} true, falls ein User mit diesem Nick existiert, false andernfalls
 */
UserAccess.prototype.exists = function(nick) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_isUserDeleted
 * @param {Number} userId
 * @return {Boolean} true, falls der User mit dieser userId gelöscht wurde, false andernfalls (auch wenn er niemals existierte)
 */
UserAccess.prototype.isUserDeleted = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_mayAccess
 * @param {Number} userId
 * @return {Boolean} true, falls der Nutzer mit UserAccess/getUserById:method geladen werden darf, false andernfalls
 */
UserAccess.prototype.mayAccess = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_getUserById
 * @param {Number} userId
 * @return {User} User, der die übergebene userId besitzt
 */
UserAccess.prototype.getUserById = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_getNick
 * @param {Number} userId
 * @return {String} Korrekt geschriebener Nickname für den User der übergebenen userId
 */
UserAccess.prototype.getNick = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserAccess.html#method_eachAccessibleUser
 * @param {Function} callback
 * @param {Object} [parameters]
 * @return {void} 
 */
UserAccess.prototype.eachAccessibleUser = function(callback, parameters) {};




/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html
 * @class Channel
 */
function Channel() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getChannelConfiguration
 * @return {ChannelConfiguration} 
 */
Channel.prototype.getChannelConfiguration = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getChannelRestrictions
 * @return {ChannelRestrictions} 
 */
Channel.prototype.getChannelRestrictions = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getChannelDesign
 * @since AppServer 87470, ChatServer 87470
 * @return {ChannelDesign} 
 */
Channel.prototype.getChannelDesign = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getOnlineUsers
 * @param {UserType} [userType]
 * @return {User[]} 
 */
Channel.prototype.getOnlineUsers = function(userType) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_isVideoChannel
 * @return {Boolean} 
 */
Channel.prototype.isVideoChannel = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getVideoChannelData
 * @return {VideoChannelData} 
 */
Channel.prototype.getVideoChannelData = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getChannelName
 * @return {String} 
 */
Channel.prototype.getChannelName = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getRootChannelName
 * @return {String} 
 */
Channel.prototype.getRootChannelName = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getTalkMode
 * @return {ChannelTalkMode} 
 */
Channel.prototype.getTalkMode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_getAllUsersWithTalkPermission
 * @param {ChannelTalkPermission} channelTalkPermission
 * @return {User[]} 
 */
Channel.prototype.getAllUsersWithTalkPermission = function(channelTalkPermission) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Channel.html#method_isVisible
 * @since AppServer 82202
 * @return {Boolean} 
 */
Channel.prototype.isVisible = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/Color.html
 * @class Color
 */
function Color() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_fromRGB
 * @static
 * @param {Number} red
 * @param {Number} green
 * @param {Number} blue
 * @return {Color} 
 */
Color.fromRGB = function(red, green, blue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_fromRGBA
 * @static
 * @param {Number} red
 * @param {Number} green
 * @param {Number} blue
 * @param {Number} alpha
 * @return {Color} 
 */
Color.fromRGBA = function(red, green, blue, alpha) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_getAlpha
 * @return {Number} 
 */
Color.prototype.getAlpha = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_getBlue
 * @return {Number} 
 */
Color.prototype.getBlue = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_getGreen
 * @return {Number} 
 */
Color.prototype.getGreen = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_getRed
 * @return {Number} 
 */
Color.prototype.getRed = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_toKCode
 * @return {String} 
 */
Color.prototype.toKCode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_asNumber
 * @return {Number} 
 */
Color.prototype.asNumber = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Color.html#method_fromNumber
 * @static
 * @param {Number} value
 * @return {Color} 
 */
Color.fromNumber = function(value) {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntryAccess.html
 * @class AppProfileEntryAccess
 */
function AppProfileEntryAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntryAccess.html#method_getAllProfileEntries
 * @return {AppProfileEntry[]} 
 */
AppProfileEntryAccess.prototype.getAllProfileEntries = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntryAccess.html#method_getAppProfileEntry
 * @param {String} userPersistenceNumberKey
 * @return {AppProfileEntry} 
 */
AppProfileEntryAccess.prototype.getAppProfileEntry = function(userPersistenceNumberKey) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntryAccess.html#method_createOrUpdateEntry
 * @param {Toplist} toplist
 * @param {ToplistDisplayType} toplistDisplayType
 * @return {AppProfileEntry} 
 */
AppProfileEntryAccess.prototype.createOrUpdateEntry = function(toplist, toplistDisplayType) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntryAccess.html#method_removeEntry
 * @param {AppProfileEntry} appProfileEntry
 * @return {void} 
 */
AppProfileEntryAccess.prototype.removeEntry = function(appProfileEntry) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelConfiguration.html
 * @class ChannelConfiguration
 */
function ChannelConfiguration() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelConfiguration.html#method_getChannelRights
 * @return {ChannelRights} 
 */
ChannelConfiguration.prototype.getChannelRights = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelConfiguration.html#method_getChannelInformation
 * @return {ChannelInformation} 
 */
ChannelConfiguration.prototype.getChannelInformation = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/PublicEventMessage.html
 * @class PublicEventMessage
 * @extends Message
 */
function PublicEventMessage() {}
PublicEventMessage.prototype = new Message;




/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistence.html
 * @class UserPersistence
 * @extends Persistence
 */
function UserPersistence() {}
UserPersistence.prototype = new Persistence;

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistence.html#method_deleteAllNumbers
 * @since AppServer 88569
 * @return {Number} Anzahl der gelöschten Zahlenwerte
 */
UserPersistence.prototype.deleteAllNumbers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistence.html#method_deleteAllObjects
 * @since AppServer 88569
 * @return {Number} Anzahl der gelöschten Objekte
 */
UserPersistence.prototype.deleteAllObjects = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistence.html#method_deleteAllStrings
 * @since AppServer 88569
 * @return {Number} Anzahl der gelöschten Zeichenketten.
 */
UserPersistence.prototype.deleteAllStrings = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistence.html#method_deleteAll
 * @since AppServer 88569
 * @return {Number} 
 */
UserPersistence.prototype.deleteAll = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html
 * @class UserPersistenceNumbers
 */
function UserPersistenceNumbers() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getSum
 * @static
 * @param {String} key
 * @return {Number} Summe
 */
UserPersistenceNumbers.getSum = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_deleteAll
 * @static
 * @param {String} key
 * @return {Number} Anzahl der gelöschten Einträge
 */
UserPersistenceNumbers.deleteAll = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getCount
 * @static
 * @param {String} key
 * @param {Object} [parameters]
 * @return {Number} 
 */
UserPersistenceNumbers.getCount = function(key, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_updateKey
 * @static
 * @param {String} oldKeyName
 * @param {String} newKeyName
 * @return {Number} Anzahl der aktualisierten Nutzer
 */
UserPersistenceNumbers.updateKey = function(oldKeyName, newKeyName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_updateValue
 * @static
 * @param {String} key
 * @param {Number} oldValue
 * @param {Number} newValue
 * @return {Number} Anzahl der Einträge, die geändert wurden
 */
UserPersistenceNumbers.updateValue = function(key, oldValue, newValue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_addNumber
 * @static
 * @param {String} key
 * @param {Number} value
 * @param {Object} [parameters]
 * @return {Number} Anzahl der Einträge, die geändert wurden
 */
UserPersistenceNumbers.addNumber = function(key, value, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getSortedEntries
 * @static
 * @param {String} key
 * @param {Object} [parameters]
 * @return {UserPersistenceNumberEntry[]} Gefundene Elemente - falls keine Elemente vorhanden sind ist das Array leer.
 */
UserPersistenceNumbers.getSortedEntries = function(key, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getSortedEntriesAdjacent
 * @static
 * @param {String} key
 * @param {User|Number} user|userId
 * @param {Object} [parameters]
 * @return {UserPersistenceNumberEntry[]} Gefundene Elemente - falls keine Elemente vorhanden sind ist das Array leer.
 */
UserPersistenceNumbers.getSortedEntriesAdjacent = function(key, userOruserId, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getPosition
 * @static
 * @param {String} key
 * @param {User|Number} user|userId
 * @param {Object} [parameters]
 * @return {Number} 
 */
UserPersistenceNumbers.getPosition = function(key, userOruserId, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getRank
 * @static
 * @param {String} key
 * @param {User|Number} user|userId
 * @param {Object} [parameters]
 * @return {Number} 
 */
UserPersistenceNumbers.getRank = function(key, userOruserId, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_each
 * @static
 * @param {String} key
 * @param {Function} callback
 * @param {Object} [parameters]
 * @return {void} 
 */
UserPersistenceNumbers.each = function(key, callback, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumbers.html#method_getAllKeys
 * @static
 * @since AppServer 82483
 * @param {String} [filterKey]
 * @return {String[]} Liste mit allen keys
 */
UserPersistenceNumbers.getAllKeys = function(filterKey) {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html
 * @class AppInfo
 */
function AppInfo() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppUid
 * @return {Number} 
 */
AppInfo.prototype.getAppUid = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getRootAppUid
 * @return {Number} 
 */
AppInfo.prototype.getRootAppUid = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppName
 * @return {String} Name der App
 */
AppInfo.prototype.getAppName = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppVersion
 * @return {String} 
 */
AppInfo.prototype.getAppVersion = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppId
 * @return {String} appId
 */
AppInfo.prototype.getAppId = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppKey
 * @return {String} appKey
 */
AppInfo.prototype.getAppKey = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppDeveloper
 * @return {User} 
 */
AppInfo.prototype.getAppDeveloper = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getAppManagers
 * @return {User[]} 
 */
AppInfo.prototype.getAppManagers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_updateApp
 * @param {String} [message]
 * @param {String} [logMessage]
 * @return {void} 
 */
AppInfo.prototype.updateApp = function(message, logMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_stopApp
 * @param {String} [message]
 * @param {String} [logMessage]
 * @return {void} 
 */
AppInfo.prototype.stopApp = function(message, logMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getTaxRate
 * @return {Number} 
 */
AppInfo.prototype.getTaxRate = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getTotalTaxKnuddelAmount
 * @return {KnuddelAmount} 
 */
AppInfo.prototype.getTotalTaxKnuddelAmount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppInfo.html#method_getMaxPayoutKnuddelAmount
 * @return {KnuddelAmount} 
 */
AppInfo.prototype.getMaxPayoutKnuddelAmount = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html
 * @class Persistence
 */
function Persistence() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_hasString
 * @param {String} key
 * @return {Boolean} 
 */
Persistence.prototype.hasString = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_setString
 * @param {String} key
 * @param {String} value
 * @return {void} 
 */
Persistence.prototype.setString = function(key, value) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_getString
 * @param {String} key
 * @param {String} [defaultValue]
 * @return {String} 
 */
Persistence.prototype.getString = function(key, defaultValue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_deleteString
 * @param {String} key
 * @return {void} 
 */
Persistence.prototype.deleteString = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_hasNumber
 * @param {String} key
 * @return {Boolean} 
 */
Persistence.prototype.hasNumber = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_setNumber
 * @param {String} key
 * @param {Number} value
 * @return {void} 
 */
Persistence.prototype.setNumber = function(key, value) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_addNumber
 * @param {String} key
 * @param {Number} value
 * @return {Number} Der neue Wert, der für key gespeichert ist.
 */
Persistence.prototype.addNumber = function(key, value) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_getNumber
 * @param {String} key
 * @param {Number} [defaultValue]
 * @return {Number} 
 */
Persistence.prototype.getNumber = function(key, defaultValue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_deleteNumber
 * @param {String} key
 * @return {void} 
 */
Persistence.prototype.deleteNumber = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_hasObject
 * @param {String} key
 * @return {Boolean} 
 */
Persistence.prototype.hasObject = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_setObject
 * @param {String} key
 * @param {Object} object
 * @return {void} 
 */
Persistence.prototype.setObject = function(key, object) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_getObject
 * @param {String} key
 * @param {Object} [defaultValue]
 * @return {Object} 
 */
Persistence.prototype.getObject = function(key, defaultValue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Persistence.html#method_deleteObject
 * @param {String} key
 * @return {void} 
 */
Persistence.prototype.deleteObject = function(key) {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html
 * @class AppContent
 */
function AppContent() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getAppViewMode
 * @return {AppViewMode} 
 */
AppContent.prototype.getAppViewMode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getHTMLFile
 * @return {HTMLFile} 
 */
AppContent.prototype.getHTMLFile = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getWidth
 * @return {Number} width
 */
AppContent.prototype.getWidth = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getHeight
 * @return {Number} height
 */
AppContent.prototype.getHeight = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getLoadConfiguration
 * @return {LoadConfiguration} 
 */
AppContent.prototype.getLoadConfiguration = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_overlayContent
 * @static
 * @param {HTMLFile} htmlFile
 * @param {Number} width
 * @param {Number} height
 * @return {AppContent} 
 */
AppContent.overlayContent = function(htmlFile, width, height) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_overlayContent
 * @static
 * @param {HTMLFile} htmlFile
 * @return {AppContent} 
 */
AppContent.overlayContent = function(htmlFile) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_popupContent
 * @static
 * @param {HTMLFile} htmlFile
 * @return {AppContent} 
 */
AppContent.popupContent = function(htmlFile) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_popupContent
 * @static
 * @param {HTMLFile} htmlFile
 * @param {Number} width
 * @param {Number} height
 * @return {AppContent} 
 */
AppContent.popupContent = function(htmlFile, width, height) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_headerbarContent
 * @static
 * @param {HTMLFile} htmlFile
 * @param {Number} height
 * @return {AppContent} 
 */
AppContent.headerbarContent = function(htmlFile, height) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_sendEvent
 * @param {String} type
 * @param {Object} [data]
 * @return {void} 
 */
AppContent.prototype.sendEvent = function(type, data) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getUsers
 * @return {User[]} 
 */
AppContent.prototype.getUsers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_getSessions
 * @return {AppContentSession[]} 
 */
AppContent.prototype.getSessions = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_replaceWithAppContent
 * @param {AppContent} newAppContent
 * @return {void} 
 */
AppContent.prototype.replaceWithAppContent = function(newAppContent) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_remove
 * @return {void} 
 */
AppContent.prototype.remove = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_addCloseListener
 * @param {Function} callback
 * @return {void} 
 */
AppContent.prototype.addCloseListener = function(callback) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_setAllowJFXBrowser
 * @param {Boolean} allowJFXBrowser
 * @return {void} 
 */
AppContent.prototype.setAllowJFXBrowser = function(allowJFXBrowser) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppContent.html#method_isAllowJFXBrowser
 * @return {Boolean} 
 */
AppContent.prototype.isAllowJFXBrowser = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ToplistAccess.html
 * @class ToplistAccess
 */
function ToplistAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistAccess.html#method_getAllToplists
 * @return {Toplist[]} 
 */
ToplistAccess.prototype.getAllToplists = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistAccess.html#method_getToplist
 * @param {String} userPersistenceNumberKey
 * @return {Toplist} 
 */
ToplistAccess.prototype.getToplist = function(userPersistenceNumberKey) {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistAccess.html#method_removeToplist
 * @param {Toplist} toplist
 * @return {void} 
 */
ToplistAccess.prototype.removeToplist = function(toplist) {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistAccess.html#method_createOrUpdateToplist
 * @param {String} userPersistenceNumberKey
 * @param {String} displayName
 * @param {Object} [parameters]
 * @return {Toplist} 
 */
ToplistAccess.prototype.createOrUpdateToplist = function(userPersistenceNumberKey, displayName, parameters) {};




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html
 * @class KnuddelsServer
 */
function KnuddelsServer() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_execute
 * @static
 * @param {String} fileName
 * @return {void} 
 */
KnuddelsServer.execute = function(fileName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_listFiles
 * @static
 * @param {String} path
 * @return {String[]} 
 */
KnuddelsServer.listFiles = function(path) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getDefaultBotUser
 * @static
 * @return {BotUser} 
 */
KnuddelsServer.getDefaultBotUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getPersistence
 * @static
 * @return {AppPersistence} 
 */
KnuddelsServer.getPersistence = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getChannel
 * @static
 * @return {Channel} 
 */
KnuddelsServer.getChannel = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppDeveloper
 * @static
 * @return {User} 
 */
KnuddelsServer.getAppDeveloper = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppId
 * @static
 * @return {String} appId
 */
KnuddelsServer.getAppId = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppName
 * @static
 * @return {String} Name der App
 */
KnuddelsServer.getAppName = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppVersion
 * @static
 * @return {String} 
 */
KnuddelsServer.getAppVersion = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getUserAccess
 * @static
 * @return {UserAccess} 
 */
KnuddelsServer.getUserAccess = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getPaymentAccess
 * @static
 * @since AppServer 108571, ChatServer 108571
 * @return {PaymentAccess} 
 */
KnuddelsServer.getPaymentAccess = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getExternalServerAccess
 * @static
 * @return {ExternalServerAccess} 
 */
KnuddelsServer.getExternalServerAccess = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_userExists
 * @static
 * @param {String} nick
 * @return {Boolean} 
 */
KnuddelsServer.userExists = function(nick) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getUserId
 * @static
 * @param {String} nick
 * @return {Number} 
 */
KnuddelsServer.getUserId = function(nick) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_canAccessUser
 * @static
 * @param {Number} userId
 * @return {Boolean} 
 */
KnuddelsServer.canAccessUser = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getNickCorrectCase
 * @static
 * @param {Number} userId
 * @return {String} 
 */
KnuddelsServer.getNickCorrectCase = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getUser
 * @static
 * @param {Number} userId
 * @return {User} 
 */
KnuddelsServer.getUser = function(userId) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_refreshHooks
 * @static
 * @return {void} 
 */
KnuddelsServer.refreshHooks = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getDefaultLogger
 * @static
 * @return {Logger} 
 */
KnuddelsServer.getDefaultLogger = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getFullImagePath
 * @static
 * @param {String} imageName
 * @return {String} Absoluter Pfad zum Einbinden der Grafik
 */
KnuddelsServer.getFullImagePath = function(imageName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getFullSystemImagePath
 * @static
 * @param {String} imageName
 * @return {String} Absoluter Pfad zum Einbinden der Grafik
 */
KnuddelsServer.getFullSystemImagePath = function(imageName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_isTestSystem
 * @static
 * @return {Boolean} 
 */
KnuddelsServer.isTestSystem = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getChatServerInfo
 * @static
 * @return {ChatServerInfo} 
 */
KnuddelsServer.getChatServerInfo = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppServerInfo
 * @static
 * @return {AppServerInfo} 
 */
KnuddelsServer.getAppServerInfo = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppInfo
 * @static
 * @return {AppInfo} 
 */
KnuddelsServer.getAppInfo = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppAccess
 * @static
 * @return {AppAccess} 
 */
KnuddelsServer.getAppAccess = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppManagers
 * @static
 * @return {User[]} 
 */
KnuddelsServer.getAppManagers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_createKnuddelPot
 * @static
 * @param {KnuddelAmount} knuddelAmount
 * @param {Object} [params]
 * @return {KnuddelPot} 
 */
KnuddelsServer.createKnuddelPot = function(knuddelAmount, params) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getKnuddelPot
 * @static
 * @param {Number} id
 * @return {KnuddelPot|null} 
 */
KnuddelsServer.getKnuddelPot = function(id) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAllKnuddelPots
 * @static
 * @return {KnuddelPot[]} 
 */
KnuddelsServer.getAllKnuddelPots = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getToplistAccess
 * @static
 * @return {ToplistAccess} 
 */
KnuddelsServer.getToplistAccess = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelsServer.html#method_getAppProfileEntryAccess
 * @static
 * @return {AppProfileEntryAccess} 
 */
KnuddelsServer.getAppProfileEntryAccess = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntry.html
 * @class AppProfileEntry
 */
function AppProfileEntry() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntry.html#method_getKey
 * @return {String} 
 */
AppProfileEntry.prototype.getKey = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntry.html#method_getDisplayType
 * @return {ToplistDisplayType} 
 */
AppProfileEntry.prototype.getDisplayType = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppProfileEntry.html#method_getToplist
 * @return {Toplist} 
 */
AppProfileEntry.prototype.getToplist = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/QuestAccess.html
 * @class QuestAccess
 */
function QuestAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/QuestAccess.html#method_getQuests
 * @since AppServer 82290, ChatServer 82290
 * @return {Quest[]} quests
 */
QuestAccess.prototype.getQuests = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/QuestAccess.html#method_hasQuest
 * @since AppServer 82290, ChatServer 82290
 * @param {String} questKey
 * @return {Boolean} 
 */
QuestAccess.prototype.hasQuest = function(questKey) {};

/**
 * @see https://developer.knuddels.de/docs/classes/QuestAccess.html#method_getQuest
 * @since AppServer 82290, ChatServer 82290
 * @param {String} questKey
 * @return {Quest|null} quest
 */
QuestAccess.prototype.getQuest = function(questKey) {};

/**
 * @see https://developer.knuddels.de/docs/classes/QuestAccess.html#method_getUser
 * @since AppServer 82290, ChatServer 82290
 * @return {User} 
 */
QuestAccess.prototype.getUser = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html
 * @class KnuddelPot
 */
function KnuddelPot() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getId
 * @return {Number} id
 */
KnuddelPot.prototype.getId = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getState
 * @return {KnuddelPotState} state
 */
KnuddelPot.prototype.getState = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getKnuddelAmountPerParticipant
 * @return {KnuddelAmount} 
 */
KnuddelPot.prototype.getKnuddelAmountPerParticipant = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getKnuddelAmountTotal
 * @return {KnuddelAmount} 
 */
KnuddelPot.prototype.getKnuddelAmountTotal = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getParticipants
 * @return {User[]} 
 */
KnuddelPot.prototype.getParticipants = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getMaxFeeMultiplier
 * @return {Number} 
 */
KnuddelPot.prototype.getMaxFeeMultiplier = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_setFee
 * @param {BotUser} feeUser
 * @param {Number} feeMultiplier
 * @return {void} 
 */
KnuddelPot.prototype.setFee = function(feeUser, feeMultiplier) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getFeeUser
 * @return {User} 
 */
KnuddelPot.prototype.getFeeUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_getFeeMultiplier
 * @return {Number} 
 */
KnuddelPot.prototype.getFeeMultiplier = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_seal
 * @return {void} 
 */
KnuddelPot.prototype.seal = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_refund
 * @param {String} [reason]
 * @return {void} 
 */
KnuddelPot.prototype.refund = function(reason) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_addWinner
 * @param {User} user
 * @param {Number} [weight]
 * @return {void} 
 */
KnuddelPot.prototype.addWinner = function(user, weight) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelPot.html#method_payout
 * @param {String} [text]
 * @return {void} 
 */
KnuddelPot.prototype.payout = function(text) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html
 * @class ExternalServerAccess
 */
function ExternalServerAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html#method_getAllAccessibleDomains
 * @return {Domain[]} 
 */
ExternalServerAccess.prototype.getAllAccessibleDomains = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html#method_canAccessURL
 * @param {String} urlString
 * @return {Boolean} 
 */
ExternalServerAccess.prototype.canAccessURL = function(urlString) {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html#method_getURL
 * @param {String} urlString
 * @param {Object} [parameters]
 * @return {void} 
 */
ExternalServerAccess.prototype.getURL = function(urlString, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html#method_postURL
 * @param {String} urlString
 * @param {Object} [parameters]
 * @return {void} 
 */
ExternalServerAccess.prototype.postURL = function(urlString, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html#method_touchURL
 * @param {String} urlString
 * @param {Object} [parameters]
 * @return {void} 
 */
ExternalServerAccess.prototype.touchURL = function(urlString, parameters) {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerAccess.html#method_callURL
 * @param {String} urlString
 * @param {Object} [parameters]
 * @return {void} 
 */
ExternalServerAccess.prototype.callURL = function(urlString, parameters) {};




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html
 * @class KnuddelTransfer
 */
function KnuddelTransfer() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_getSender
 * @return {User} 
 */
KnuddelTransfer.prototype.getSender = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_getReceiver
 * @return {BotUser} 
 */
KnuddelTransfer.prototype.getReceiver = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_getKnuddelAmount
 * @return {KnuddelAmount} 
 */
KnuddelTransfer.prototype.getKnuddelAmount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_getTransferReason
 * @return {String} 
 */
KnuddelTransfer.prototype.getTransferReason = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_reject
 * @param {String} reason
 * @return {void} 
 */
KnuddelTransfer.prototype.reject = function(reason) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_accept
 * @return {void} 
 */
KnuddelTransfer.prototype.accept = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_canAddToPot
 * @param {KnuddelPot} pot
 * @return {Boolean} 
 */
KnuddelTransfer.prototype.canAddToPot = function(pot) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_addToPot
 * @param {KnuddelPot} knuddelPot
 * @return {void} 
 */
KnuddelTransfer.prototype.addToPot = function(knuddelPot) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransfer.html#method_isProcessed
 * @return {Boolean} 
 */
KnuddelTransfer.prototype.isProcessed = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/Domain.html
 * @class Domain
 */
function Domain() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Domain.html#method_getDomainName
 * @return {String} 
 */
Domain.prototype.getDomainName = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/UserType.html
 * @class UserType
 */
function UserType() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserType.html#property_AppBot
 * Bot der App.

* @type {UserType}
*/
UserType.AppBot = 'AppBot';
/**
 * @see https://developer.knuddels.de/docs/classes/UserType.html#property_SystemBot
 * Bot des Knuddels-Chatsystems.

* @type {UserType}
*/
UserType.SystemBot = 'SystemBot';
/**
 * @see https://developer.knuddels.de/docs/classes/UserType.html#property_Human
 * Menschlicher User.

* @type {UserType}
*/
UserType.Human = 'Human';



/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceStrings.html
 * @class UserPersistenceStrings
 */
function UserPersistenceStrings() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceStrings.html#method_exists
 * @static
 * @since AppServer 88571
 * @param {String} key
 * @param {String} value
 * @param {Boolean} [ignoreCase]
 * @return {Boolean} Information, ob Eintrag existiert.
 */
UserPersistenceStrings.exists = function(key, value, ignoreCase) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceStrings.html#method_deleteAll
 * @static
 * @since AppServer 82478
 * @param {String} key
 * @return {Number} Anzahl der gelöschten Einträge
 */
UserPersistenceStrings.deleteAll = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceStrings.html#method_getAllKeys
 * @static
 * @since AppServer 82483
 * @param {String} [filterKey]
 * @return {String[]} Liste mit allen keys
 */
UserPersistenceStrings.getAllKeys = function(filterKey) {};




/**
 * @see https://developer.knuddels.de/docs/classes/BotUser.html
 * @class BotUser
 * @extends User
 */
function BotUser() {}
BotUser.prototype = new User;

/**
 * @see https://developer.knuddels.de/docs/classes/BotUser.html#method_sendPublicMessage
 * @param {String} message
 * @return {void} 
 */
BotUser.prototype.sendPublicMessage = function(message) {};

/**
 * @see https://developer.knuddels.de/docs/classes/BotUser.html#method_sendPublicActionMessage
 * @param {String} actionMessage
 * @return {void} 
 */
BotUser.prototype.sendPublicActionMessage = function(actionMessage) {};

/**
 * @see https://developer.knuddels.de/docs/classes/BotUser.html#method_sendPrivateMessage
 * @param {String} message
 * @param {User[]} [users]
 * @return {void} 
 */
BotUser.prototype.sendPrivateMessage = function(message, users) {};

/**
 * @see https://developer.knuddels.de/docs/classes/BotUser.html#method_sendPostMessage
 * @param {String} topic
 * @param {String} text
 * @param {User} [receivingUser]
 * @return {void} 
 */
BotUser.prototype.sendPostMessage = function(topic, text, receivingUser) {};

/**
 * @see https://developer.knuddels.de/docs/classes/BotUser.html#method_transferKnuddel
 * @param {User|KnuddelAccount} receivingUserOrAccount
 * @param {KnuddelAmount} knuddelAmount
 * @param {Object} [parameters]
 * @return {void} 
 */
BotUser.prototype.transferKnuddel = function(receivingUserOrAccount, knuddelAmount, parameters) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ClientType.html
 * @class ClientType
 */
function ClientType() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ClientType.html#property_Applet
 * Der User ist mit dem Java Applet im Chat.

* @type {ClientType}
*/
ClientType.Applet = 'Applet';
/**
 * @see https://developer.knuddels.de/docs/classes/ClientType.html#property_Browser
 * Der User ist mit dem Browser im Chat (Mini-Chat, HTML-Chat).

* @type {ClientType}
*/
ClientType.Browser = 'Browser';
/**
 * @see https://developer.knuddels.de/docs/classes/ClientType.html#property_Android
 * Der User ist mit der Android-App im Chat.

* @type {ClientType}
*/
ClientType.Android = 'Android';
/**
 * @see https://developer.knuddels.de/docs/classes/ClientType.html#property_IOS
 * Der User ist mit der iOS-App im Chat.

* @type {ClientType}
*/
ClientType.IOS = 'IOS';
/**
 * @see https://developer.knuddels.de/docs/classes/ClientType.html#property_Offline
 * Der User ist nicht im Chat.

* @type {ClientType}
*/
ClientType.Offline = 'Offline';



/**
 * @see https://developer.knuddels.de/docs/classes/User.html
 * @class User
 */
function User() {}

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getUserId
 * @return {Number} 
 */
User.prototype.getUserId = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getNick
 * @return {String} 
 */
User.prototype.getNick = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getAge
 * @return {Number} 
 */
User.prototype.getAge = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getGender
 * @return {Gender} 
 */
User.prototype.getGender = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getRegDate
 * @return {Date} 
 */
User.prototype.getRegDate = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getUserStatus
 * @return {UserStatus} 
 */
User.prototype.getUserStatus = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getUserType
 * @return {UserType} 
 */
User.prototype.getUserType = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getClientType
 * @return {ClientType} 
 */
User.prototype.getClientType = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_canShowAppViewMode
 * @param {AppViewMode} mode
 * @return {Boolean} 
 */
User.prototype.canShowAppViewMode = function(mode) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_canSendAppContent
 * @param {AppContent} appContent
 * @return {Boolean} 
 */
User.prototype.canSendAppContent = function(appContent) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isInTeam
 * @param {String} teamName
 * @param {String} [subTeamName]
 * @return {Boolean} 
 */
User.prototype.isInTeam = function(teamName, subTeamName) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getPersistence
 * @return {UserPersistence} 
 */
User.prototype.getPersistence = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_sendPrivateMessage
 * @param {String} message
 * @return {void} 
 */
User.prototype.sendPrivateMessage = function(message) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_sendPostMessage
 * @param {String} topic
 * @param {String} text
 * @return {void} 
 */
User.prototype.sendPostMessage = function(topic, text) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isChannelOwner
 * @return {Boolean} 
 */
User.prototype.isChannelOwner = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isLikingChannel
 * @return {Boolean} 
 */
User.prototype.isLikingChannel = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isChannelCoreUser
 * @since AppServer 92701, ChatServer 92701
 * @return {Boolean} 
 */
User.prototype.isChannelCoreUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isAppManager
 * @return {Boolean} 
 */
User.prototype.isAppManager = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isMuted
 * @return {Boolean} 
 */
User.prototype.isMuted = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isColorMuted
 * @return {Boolean} 
 */
User.prototype.isColorMuted = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isLocked
 * @return {Boolean} 
 */
User.prototype.isLocked = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isChannelModerator
 * @return {Boolean} 
 */
User.prototype.isChannelModerator = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isEventModerator
 * @return {Boolean} 
 */
User.prototype.isEventModerator = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isAppDeveloper
 * @return {Boolean} 
 */
User.prototype.isAppDeveloper = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getProfileLink
 * @param {String} [displayText]
 * @return {String} 
 */
User.prototype.getProfileLink = function(displayText) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isOnlineInChannel
 * @return {Boolean} 
 */
User.prototype.isOnlineInChannel = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getKnuddelAmount
 * @return {KnuddelAmount} Anzahl der Knuddel
 */
User.prototype.getKnuddelAmount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isOnline
 * @return {Boolean} true, falls der Nutzer online ist.
 */
User.prototype.isOnline = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getReadme
 * @return {String} Readme des Nutzers
 */
User.prototype.getReadme = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getOnlineMinutes
 * @return {Number} 
 */
User.prototype.getOnlineMinutes = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isAway
 * @return {Boolean} 
 */
User.prototype.isAway = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_hasProfilePhoto
 * @return {Boolean} 
 */
User.prototype.hasProfilePhoto = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_sendAppContent
 * @param {AppContent} appContent
 * @return {AppContentSession} appContentSession, null, falls die interne Prüfung von canShowAppContent false geliefert hat
 */
User.prototype.sendAppContent = function(appContent) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getAppContentSessions
 * @return {AppContentSession[]} 
 */
User.prototype.getAppContentSessions = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getAppContentSession
 * @param {AppViewMode} appViewMode
 * @return {AppContentSession} appContentSession für übergebenen AppViewMode, ansonsten null
 */
User.prototype.getAppContentSession = function(appViewMode) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_sendEvent
 * @param {String} type
 * @param {Object} data
 * @return {void} 
 */
User.prototype.sendEvent = function(type, data) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_removeAppContent
 * @return {void} 
 */
User.prototype.removeAppContent = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_equals
 * @param {User} user
 * @return {Boolean} 
 */
User.prototype.equals = function(user) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getProfilePhoto
 * @param {Number} width
 * @param {Number} height
 * @return {String} 
 */
User.prototype.getProfilePhoto = function(width, height) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getQuestAccess
 * @since AppServer 82290, ChatServer 82290
 * @return {QuestAccess} questAccess
 */
User.prototype.getQuestAccess = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isStreamingVideo
 * @return {Boolean} 
 */
User.prototype.isStreamingVideo = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getKnuddelAccount
 * @return {KnuddelAccount} 
 */
User.prototype.getKnuddelAccount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getChannelTalkPermission
 * @return {ChannelTalkPermission} 
 */
User.prototype.getChannelTalkPermission = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isProfilePhotoVerified
 * @return {Boolean} 
 */
User.prototype.isProfilePhotoVerified = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_isAgeVerified
 * @return {Boolean} 
 */
User.prototype.isAgeVerified = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_addNicklistIcon
 * @param {String} imagePath
 * @param {Number} imageWidth
 * @return {void} 
 */
User.prototype.addNicklistIcon = function(imagePath, imageWidth) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_removeNicklistIcon
 * @param {String} imagePath
 * @return {void} 
 */
User.prototype.removeNicklistIcon = function(imagePath) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_triggerDice
 * @since AppServer 89159, ChatServer 89159
 * @param {DiceConfiguration} diceConfiguration
 * @return {void} 
 */
User.prototype.triggerDice = function(diceConfiguration) {};

/**
 * @see https://developer.knuddels.de/docs/classes/User.html#method_getAuthenticityClassification
 * @since AppServer 94663, ChatServer 94663
 * @return {AuthenticityClassification} 
 */
User.prototype.getAuthenticityClassification = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/Message.html
 * @class Message
 */
function Message() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Message.html#method_getAuthor
 * @return {User} 
 */
Message.prototype.getAuthor = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Message.html#method_getText
 * @return {String} 
 */
Message.prototype.getText = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Message.html#method_getCreationDate
 * @return {Date} 
 */
Message.prototype.getCreationDate = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ServerInfo.html
 * @class ServerInfo
 */
function ServerInfo() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ServerInfo.html#method_getServerId
 * @return {String} 
 */
ServerInfo.prototype.getServerId = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ServerInfo.html#method_getRevision
 * @return {Number} 
 */
ServerInfo.prototype.getRevision = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html
 * @class DiceConfigurationFactory
 */
function DiceConfigurationFactory() {}

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_addDice
 * @param {Dice} dice
 * @return {void} 
 */
DiceConfigurationFactory.prototype.addDice = function(dice) {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_computeCurrentDiceCount
 * @return {Number} 
 */
DiceConfigurationFactory.prototype.computeCurrentDiceCount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_setUseOpenThrow
 * @param {Boolean} shouldUseOpenThrow
 * @return {void} 
 */
DiceConfigurationFactory.prototype.setUseOpenThrow = function(shouldUseOpenThrow) {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_setUseOpenThrows
 * @param {Boolean} shouldUseOpenThrow
 * @return {void} 
 */
DiceConfigurationFactory.prototype.setUseOpenThrows = function(shouldUseOpenThrow) {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_setShouldUsePrivateThrow
 * @param {Boolean} shouldUsePrivateThrow
 * @return {void} 
 */
DiceConfigurationFactory.prototype.setShouldUsePrivateThrow = function(shouldUsePrivateThrow) {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_getDiceConfiguration
 * @return {DiceConfiguration} 
 */
DiceConfigurationFactory.prototype.getDiceConfiguration = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfigurationFactory.html#method_fromString
 * @static
 * @param {String} diceConfigurationString
 * @return {DiceConfiguration} 
 */
DiceConfigurationFactory.fromString = function(diceConfigurationString) {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppViewMode.html
 * @class AppViewMode
 */
function AppViewMode() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppViewMode.html#property_Overlay
 * Zum Öffnen eines Overlay durch das HTML User Interface

* @type {AppViewMode}
*/
AppViewMode.Overlay = 'Overlay';
/**
 * @see https://developer.knuddels.de/docs/classes/AppViewMode.html#property_Popup
 * Zum Öffnen eines Popups durch das HTML User Interface

* @type {AppViewMode}
*/
AppViewMode.Popup = 'Popup';
/**
 * @see https://developer.knuddels.de/docs/classes/AppViewMode.html#property_Headerbar
 * Zum Öffnen einer Headerbar durch das HTML User Interface

* @type {AppViewMode}
*/
AppViewMode.Headerbar = 'Headerbar';



/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumberEntry.html
 * @class UserPersistenceNumberEntry
 */
function UserPersistenceNumberEntry() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumberEntry.html#method_getUser
 * @return {User} 
 */
UserPersistenceNumberEntry.prototype.getUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumberEntry.html#method_getValue
 * @return {Number} 
 */
UserPersistenceNumberEntry.prototype.getValue = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumberEntry.html#method_getRank
 * @return {Number} 
 */
UserPersistenceNumberEntry.prototype.getRank = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceNumberEntry.html#method_getPosition
 * @return {Number} 
 */
UserPersistenceNumberEntry.prototype.getPosition = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/PublicMessage.html
 * @class PublicMessage
 * @extends Message
 */
function PublicMessage() {}
PublicMessage.prototype = new Message;




/**
 * @see https://developer.knuddels.de/docs/classes/Logger.html
 * @class Logger
 */
function Logger() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Logger.html#method_debug
 * @param {Object} msg
 * @return {void} 
 */
Logger.prototype.debug = function(msg) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Logger.html#method_info
 * @param {Object} msg
 * @return {void} 
 */
Logger.prototype.info = function(msg) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Logger.html#method_warn
 * @param {Object} msg
 * @return {void} 
 */
Logger.prototype.warn = function(msg) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Logger.html#method_error
 * @param {Object} msg
 * @return {void} 
 */
Logger.prototype.error = function(msg) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Logger.html#method_fatal
 * @param {Object} msg
 * @return {void} 
 */
Logger.prototype.fatal = function(msg) {};




/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html
 * @class DiceConfiguration
 */
function DiceConfiguration() {}

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html#method_isUsingOpenThrow
 * @return {Boolean} 
 */
DiceConfiguration.prototype.isUsingOpenThrow = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html#method_isUsingPrivateThrow
 * @return {Boolean} 
 */
DiceConfiguration.prototype.isUsingPrivateThrow = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html#method_getDices
 * @return {Dice[]} 
 */
DiceConfiguration.prototype.getDices = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html#method_equals
 * @param {DiceConfiguration} diceConfiguration
 * @return {Boolean} 
 */
DiceConfiguration.prototype.equals = function(diceConfiguration) {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html#method_getChatCommand
 * @since AppServer 82248
 * @return {String} 
 */
DiceConfiguration.prototype.getChatCommand = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceConfiguration.html#method_toString
 * @since AppServer 108781
 * @return {String} z.B. "5W5 + W6"
 */
DiceConfiguration.prototype.toString = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html
 * @class ChannelTalkPermission
 */
function ChannelTalkPermission() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html#property_NotInChannel
 * Der User ist gerade nicht im Channel,
 * daher ist die ChannelTalkPermission nicht bekannnt.

* @type {ChannelTalkPermission}
*/
ChannelTalkPermission.NotInChannel = 'NotInChannel';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html#property_Default
 * Der User hat keine speziellen Rederechte.
 * Beim ChannelTalkMode Default können diese User
 * Nachrichten verfassen:

* @type {ChannelTalkPermission}
*/
ChannelTalkPermission.Default = 'Default';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html#property_TalkOnce
 * Der User kann eine öffentliche Nachricht verfassen. Danach wechselt die
 * ChannelTalkPermission automatisch auf Default.

* @type {ChannelTalkPermission}
*/
ChannelTalkPermission.TalkOnce = 'TalkOnce';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html#property_TalkPermanent
 * Der User kann permanent öffentliche Nachrichten verfassen.

* @type {ChannelTalkPermission}
*/
ChannelTalkPermission.TalkPermanent = 'TalkPermanent';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html#property_VIP
 * Der User ist VIP und kann öffentliche Nachrichten verfassen,
 * die farbig und groß dargestellt werden.

* @type {ChannelTalkPermission}
*/
ChannelTalkPermission.VIP = 'VIP';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkPermission.html#property_Moderator
 * Der User ist VIP und kann öffentliche Nachrichten verfassen,
 * die farbig und groß dargestellt werden. Moderatoren haben zudem weitere Möglichkeiten, die in der
 * Anleitung zum Moderationssystem
 * nachgelesen werden können.

* @type {ChannelTalkPermission}
*/
ChannelTalkPermission.Moderator = 'Moderator';



/**
 * @see https://developer.knuddels.de/docs/classes/OwnAppInstance.html
 * @class OwnAppInstance
 */
function OwnAppInstance() {}

/**
 * @see https://developer.knuddels.de/docs/classes/OwnAppInstance.html#method_getOnlineUsers
 * @since AppServer 82560
 * @param {AppInstance} otherAppInstance
 * @param {UserType} [userType]
 * @return {User[]} 
 */
OwnAppInstance.prototype.getOnlineUsers = function(otherAppInstance, userType) {};




/**
 * @see https://developer.knuddels.de/docs/classes/HTMLFile.html
 * @class HTMLFile
 */
function HTMLFile() {}

/**
 * @see https://developer.knuddels.de/docs/classes/HTMLFile.html#method_HTMLFile
 * @param {String} assetPath
 * @param {Json} [pageData]
 * @return {HTMLFile} 
 */
HTMLFile.prototype.HTMLFile = function(assetPath, pageData) {};

/**
 * @see https://developer.knuddels.de/docs/classes/HTMLFile.html#method_getAssetPath
 * @return {String} assetPath
 */
HTMLFile.prototype.getAssetPath = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/HTMLFile.html#method_getPageData
 * @return {Json} pageData
 */
HTMLFile.prototype.getPageData = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/PaymentAccess.html
 * @class PaymentAccess
 * @since AppServer 108571, ChatServer 108571
 */
function PaymentAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/PaymentAccess.html#method_startKnuddelPurchase
 * @since AppServer 108571, ChatServer 108571
 * @param {User} user
 * @param {KnuddelAmount} amount
 * @param {Object} [parameters]
 * @return {void} 
 */
PaymentAccess.prototype.startKnuddelPurchase = function(user, amount, parameters) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerResponse.html
 * @class ExternalServerResponse
 */
function ExternalServerResponse() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerResponse.html#method_getURLString
 * @return {String} 
 */
ExternalServerResponse.prototype.getURLString = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerResponse.html#method_getResponseCode
 * @return {Number} var responseCode = externalServerResponse.getResponseCode(); // z.B. 200
 */
ExternalServerResponse.prototype.getResponseCode = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerResponse.html#method_getHeaderFieldNames
 * @since AppServer 108668
 * @return {String[]} 
 */
ExternalServerResponse.prototype.getHeaderFieldNames = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerResponse.html#method_getHeaderFields
 * @return {Object} 
 */
ExternalServerResponse.prototype.getHeaderFields = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ExternalServerResponse.html#method_getHeaderFieldValues
 * @since AppServer 108668
 * @param {String} headerFieldName
 * @return {String[]} 
 */
ExternalServerResponse.prototype.getHeaderFieldValues = function(headerFieldName) {};




/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html
 * @class LoadConfiguration
 */
function LoadConfiguration() {}

/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html#method_setBackgroundColor
 * @param {Color} color
 * @return {void} 
 */
LoadConfiguration.prototype.setBackgroundColor = function(color) {};

/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html#method_setBackgroundImage
 * @param {String} imageUrl
 * @return {void} 
 */
LoadConfiguration.prototype.setBackgroundImage = function(imageUrl) {};

/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html#method_setText
 * @param {String} text
 * @return {void} 
 */
LoadConfiguration.prototype.setText = function(text) {};

/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html#method_setLoadingIndicatorImage
 * @param {String} imageUrl
 * @return {void} 
 */
LoadConfiguration.prototype.setLoadingIndicatorImage = function(imageUrl) {};

/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html#method_setForegroundColor
 * @param {Color} color
 * @return {void} 
 */
LoadConfiguration.prototype.setForegroundColor = function(color) {};

/**
 * @see https://developer.knuddels.de/docs/classes/LoadConfiguration.html#method_setEnabled
 * @param {Boolean} enabled
 * @return {void} 
 */
LoadConfiguration.prototype.setEnabled = function(enabled) {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelDesign.html
 * @class ChannelDesign
 * @since AppServer 87470, ChatServer 87470
 */
function ChannelDesign() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelDesign.html#method_getDefaultFontSize
 * @since AppServer 87470, ChatServer 87470
 * @return {Number} default font size
 */
ChannelDesign.prototype.getDefaultFontSize = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelDesign.html#method_getDefaultFontColor
 * @since AppServer 87470, ChatServer 87470
 * @return {Color} default font color
 */
ChannelDesign.prototype.getDefaultFontColor = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelDesign.html#method_getBackgroundColor
 * @since AppServer 87470, ChatServer 87470
 * @return {Color} background color
 */
ChannelDesign.prototype.getBackgroundColor = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/PublicActionMessage.html
 * @class PublicActionMessage
 * @extends Message
 */
function PublicActionMessage() {}
PublicActionMessage.prototype = new Message;




/**
 * @see https://developer.knuddels.de/docs/classes/VideoChannelData.html
 * @class VideoChannelData
 */
function VideoChannelData() {}

/**
 * @see https://developer.knuddels.de/docs/classes/VideoChannelData.html#method_getStreamingVideoUsers
 * @return {User[]} 
 */
VideoChannelData.prototype.getStreamingVideoUsers = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkMode.html
 * @class ChannelTalkMode
 */
function ChannelTalkMode() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkMode.html#property_Everyone
 * Jeder darf gerade im Channel schreiben.

* @type {ChannelTalkMode}
*/
ChannelTalkMode.Everyone = 'Everyone';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkMode.html#property_OnlyWithTalkPermission
 * Nur Personen, die besondere Rederechte haben dürfen gerade im Channel schreiben.

* @type {ChannelTalkMode}
*/
ChannelTalkMode.OnlyWithTalkPermission = 'OnlyWithTalkPermission';
/**
 * @see https://developer.knuddels.de/docs/classes/ChannelTalkMode.html#property_FilteredByModerators
 * Nur Personen, die besondere Rederechte haben dürfen gerade im Channel schreiben.
 * Die Nachrichten aller anderen Nutzer werden gefiltert und ggf. von den Moderatoren zugelassen.

* @type {ChannelTalkMode}
*/
ChannelTalkMode.FilteredByModerators = 'FilteredByModerators';



/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRestrictions.html
 * @class ChannelRestrictions
 */
function ChannelRestrictions() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRestrictions.html#method_getMutedUsers
 * @return {User[]} 
 */
ChannelRestrictions.prototype.getMutedUsers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRestrictions.html#method_getColorMutedUsers
 * @return {User[]} 
 */
ChannelRestrictions.prototype.getColorMutedUsers = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRestrictions.html#method_getLockedUsers
 * @return {User[]} 
 */
ChannelRestrictions.prototype.getLockedUsers = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransferDisplayType.html
 * @class KnuddelTransferDisplayType
 */
function KnuddelTransferDisplayType() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransferDisplayType.html#property_Public
 * Nachricht wird öffentlich angezeigt.

* @type {KnuddelTransferDisplayType}
*/
KnuddelTransferDisplayType.Public = 'Public';
/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransferDisplayType.html#property_Private
 * Nachricht wird privat angezeigt.

* @type {KnuddelTransferDisplayType}
*/
KnuddelTransferDisplayType.Private = 'Private';
/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelTransferDisplayType.html#property_Post
 * Nachricht wird als /m zugestellt.

* @type {KnuddelTransferDisplayType}
*/
KnuddelTransferDisplayType.Post = 'Post';



/**
 * @see https://developer.knuddels.de/docs/classes/Dice.html
 * @class Dice
 */
function Dice() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Dice.html#method_Dice
 * @param {Number} [count]
 * @param {Number} value
 * @return {Dice} 
 */
Dice.prototype.Dice = function(count, value) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Dice.html#method_getAmount
 * @return {Number} 
 */
Dice.prototype.getAmount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/Dice.html#method_getNumberOfSides
 * @return {Number} 
 */
Dice.prototype.getNumberOfSides = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRights.html
 * @class ChannelRights
 */
function ChannelRights() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRights.html#method_getChannelOwners
 * @return {User[]} 
 */
ChannelRights.prototype.getChannelOwners = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRights.html#method_getChannelModerators
 * @return {User[]} 
 */
ChannelRights.prototype.getChannelModerators = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelRights.html#method_getEventModerators
 * @return {User[]} 
 */
ChannelRights.prototype.getEventModerators = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceObjects.html
 * @class UserPersistenceObjects
 */
function UserPersistenceObjects() {}

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceObjects.html#method_deleteAll
 * @static
 * @since AppServer 82478
 * @param {String} key
 * @return {Number} Anzahl der gelöschten Einträge
 */
UserPersistenceObjects.deleteAll = function(key) {};

/**
 * @see https://developer.knuddels.de/docs/classes/UserPersistenceObjects.html#method_getAllKeys
 * @static
 * @since AppServer 82483
 * @param {String} [filterKey]
 * @return {String[]} Liste mit allen keys
 */
UserPersistenceObjects.getAllKeys = function(filterKey) {};




/**
 * @see https://developer.knuddels.de/docs/classes/Quest.html
 * @class Quest
 */
function Quest() {}

/**
 * @see https://developer.knuddels.de/docs/classes/Quest.html#method_setSolved
 * @since AppServer 82290, ChatServer 82290
 * @param {Number} [count]
 * @return {void} 
 */
Quest.prototype.setSolved = function(count) {};

/**
 * @see https://developer.knuddels.de/docs/classes/Quest.html#method_getQuestKey
 * @since AppServer 82290, ChatServer 82290
 * @return {String} questKey
 */
Quest.prototype.getQuestKey = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppAccess.html
 * @class AppAccess
 */
function AppAccess() {}

/**
 * @see https://developer.knuddels.de/docs/classes/AppAccess.html#method_getOwnInstance
 * @return {AppInstance} Instanz der eigenen App
 */
AppAccess.prototype.getOwnInstance = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppAccess.html#method_getAllRunningAppsInChannel
 * @since AppServer 82904
 * @param {Boolean} [includeSelf]
 * @return {AppInstance[]} Instanzen der anderen laufenden Apps
 */
AppAccess.prototype.getAllRunningAppsInChannel = function(includeSelf) {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppAccess.html#method_getRunningAppInChannel
 * @since AppServer 82904
 * @param {String} appId
 * @return {AppInstance|null} Instanz der anderen laufenden App oder null
 */
AppAccess.prototype.getRunningAppInChannel = function(appId) {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppServerInfo.html
 * @class AppServerInfo
 * @extends ServerInfo
 */
function AppServerInfo() {}
AppServerInfo.prototype = new ServerInfo;




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html
 * @class KnuddelAmount
 */
function KnuddelAmount() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_KnuddelAmount
 * @param {Number} knuddel
 * @return {KnuddelAmount} 
 */
KnuddelAmount.prototype.KnuddelAmount = function(knuddel) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_fromCents
 * @static
 * @param {Number} knuddel
 * @return {KnuddelAmount} 
 */
KnuddelAmount.fromCents = function(knuddel) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_fromKnuddel
 * @static
 * @param {Number} knuddel
 * @return {KnuddelAmount} 
 */
KnuddelAmount.fromKnuddel = function(knuddel) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_getKnuddelCents
 * @return {Number} 
 */
KnuddelAmount.prototype.getKnuddelCents = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_asNumber
 * @return {Number} 
 */
KnuddelAmount.prototype.asNumber = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_negate
 * @return {KnuddelAmount} 
 */
KnuddelAmount.prototype.negate = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAmount.html#method_isNegative
 * @return {Boolean} 
 */
KnuddelAmount.prototype.isNegative = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/DiceResult.html
 * @class DiceResult
 */
function DiceResult() {}

/**
 * @see https://developer.knuddels.de/docs/classes/DiceResult.html#method_getDiceConfiguration
 * @return {DiceConfiguration} 
 */
DiceResult.prototype.getDiceConfiguration = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceResult.html#method_getSingleDiceResults
 * @return {SingleDiceResult[]} 
 */
DiceResult.prototype.getSingleDiceResults = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceResult.html#method_totalSum
 * @return {Number} 
 */
DiceResult.prototype.totalSum = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceResult.html#method_toString
 * @since AppServer 108781
 * @return {String} z.B. "5W5 + W6: 5> 4, 4, 3, 4, 5> 3, 1 = 29"
 */
DiceResult.prototype.toString = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/AppPersistence.html
 * @class AppPersistence
 * @extends Persistence
 */
function AppPersistence() {}
AppPersistence.prototype = new Persistence;

/**
 * @see https://developer.knuddels.de/docs/classes/AppPersistence.html#method_getDatabaseFileSize
 * @return {Number} 
 */
AppPersistence.prototype.getDatabaseFileSize = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/AppPersistence.html#method_getDatabaseFileSizeLimit
 * @return {Number} 
 */
AppPersistence.prototype.getDatabaseFileSizeLimit = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html
 * @class ToplistRankChangeEvent
 */
function ToplistRankChangeEvent() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getToplist
 * @return {Toplist} 
 */
ToplistRankChangeEvent.prototype.getToplist = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getOldRank
 * @return {Number} -1, falls der Nutzer vorher keinen Rang hatte.
 */
ToplistRankChangeEvent.prototype.getOldRank = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getNewRank
 * @return {Number} 
 */
ToplistRankChangeEvent.prototype.getNewRank = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getUser
 * @return {User} 
 */
ToplistRankChangeEvent.prototype.getUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getUsersOvertook
 * @return {User[]} 
 */
ToplistRankChangeEvent.prototype.getUsersOvertook = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getOldValue
 * @return {Number} 
 */
ToplistRankChangeEvent.prototype.getOldValue = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ToplistRankChangeEvent.html#method_getNewValue
 * @return {Number} 
 */
ToplistRankChangeEvent.prototype.getNewValue = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/ChannelJoinPermission.html
 * @class ChannelJoinPermission
 */
function ChannelJoinPermission() {}

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelJoinPermission.html#method_accepted
 * @static
 * @return {ChannelJoinPermission} 
 */
ChannelJoinPermission.accepted = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/ChannelJoinPermission.html#method_denied
 * @static
 * @param {String} denyReason
 * @return {ChannelJoinPermission} 
 */
ChannelJoinPermission.denied = function(denyReason) {};




/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html
 * @class KnuddelAccount
 */
function KnuddelAccount() {}

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_getKnuddelAmount
 * @return {KnuddelAmount} 
 */
KnuddelAccount.prototype.getKnuddelAmount = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_getKnuddelAmountUsed
 * @return {KnuddelAmount} 
 */
KnuddelAccount.prototype.getKnuddelAmountUsed = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_getKnuddelAmountUnused
 * @return {KnuddelAmount} 
 */
KnuddelAccount.prototype.getKnuddelAmountUnused = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_getTotalKnuddelAmountAppToUser
 * @return {KnuddelAmount} 
 */
KnuddelAccount.prototype.getTotalKnuddelAmountAppToUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_getTotalKnuddelAmountUserToApp
 * @return {KnuddelAmount} 
 */
KnuddelAccount.prototype.getTotalKnuddelAmountUserToApp = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_hasEnough
 * @param {KnuddelAmount} knuddelAmount
 * @return {Boolean} 
 */
KnuddelAccount.prototype.hasEnough = function(knuddelAmount) {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_getUser
 * @return {User} 
 */
KnuddelAccount.prototype.getUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/KnuddelAccount.html#method_use
 * @param {KnuddelAmount} knuddelAmount
 * @param {String} displayReasonText
 * @param {Object} [parameters]
 * @return {void} 
 */
KnuddelAccount.prototype.use = function(knuddelAmount, displayReasonText, parameters) {};




/**
 * @see https://developer.knuddels.de/docs/classes/DiceEvent.html
 * @class DiceEvent
 */
function DiceEvent() {}

/**
 * @see https://developer.knuddels.de/docs/classes/DiceEvent.html#method_getUser
 * @return {User} 
 */
DiceEvent.prototype.getUser = function() {};

/**
 * @see https://developer.knuddels.de/docs/classes/DiceEvent.html#method_getDiceResult
 * @return {DiceResult} 
 */
DiceEvent.prototype.getDiceResult = function() {};




/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html
 * @class RandomOperations
 */
function RandomOperations() {}

/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html#method_nextInt
 * @static
 * @param {Number} [minValue]
 * @param {Number} maxValue
 * @return {Number} 
 */
RandomOperations.nextInt = function(minValue, maxValue) {};

/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html#method_nextInts
 * @static
 * @param {Number} [minValue]
 * @param {Number} maxValue
 * @param {Number} count
 * @param {Boolean} onlyDifferentNumbers
 * @return {Number[]} 
 */
RandomOperations.nextInts = function(minValue, maxValue, count, onlyDifferentNumbers) {};

/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html#method_flipTrue
 * @static
 * @param {Number} truePropability
 * @return {Boolean} 
 */
RandomOperations.flipTrue = function(truePropability) {};

/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html#method_getRandomObject
 * @static
 * @param {Object[]} objects
 * @return {Object} 
 */
RandomOperations.getRandomObject = function(objects) {};

/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html#method_shuffleObjects
 * @static
 * @param {Object[]} objects
 * @return {Object[]} 
 */
RandomOperations.shuffleObjects = function(objects) {};

/**
 * @see https://developer.knuddels.de/docs/classes/RandomOperations.html#method_getRandomString
 * @static
 * @since AppServer 92699
 * @param {Number} length
 * @param {String} [allowedCharacters]
 * @return {String} Zeichenkette mit gewünschter Länge
 */
RandomOperations.getRandomString = function(length, allowedCharacters) {};




