function Module() {
    K3AF.debug('Creating instance for '+ this.toString() +' | ' + JSON.stringify(getStacktrace()));
    this.visible = true;
    this._blockedModules = [];
    this.priority = 0;
    this.isLoggingFunctions = false;
    this.PERSISTENCE_KEYS = [];
};


Module.prototype.F_OnActivated = function() {};
Module.prototype.F_OnDeactivated = function() {};
Module.prototype.F_OnUpdate = function(){};

/**
 *
 * @returns {ModulePersistence}
 */
Module.prototype.getPersistence = function() {
    if(typeof this._persistence === "undefined") {
        /**
         *
         * @type {ModulePersistence}
         * @private
         */
        this._persistence = new ModulePersistence(this);
    }
    return this._persistence;
};

Module.prototype.isActivated = function isActivated() {
    return this.getPersistence().getNumber('activated',0)===1;
};

/**
 *
 * @param {User} user
 * @returns {boolean}
 */
Module.prototype.deactivate = function deactivate(user) {
    this.getPersistence().setNumber('activated', 0);
    this.F_OnDeactivated();
    ModuleManager.self.refreshHooks();
    return true;
};

/**
 *
 * @param {User} user
 * @returns {boolean}
 */
Module.prototype.activate = function activate(user) {
    var blocked = this._blockedModules;
    for(var i = 0; i < blocked.length; i++) {
        var mod = ModuleManager.self.findModule(blocked[i]);
        if(mod != null && mod.isActivated()) {
            user._sendPrivateMessage('_$THISMOD_ konnte nicht aktiviert werden, da ein Konflikt mit _$OTHERMOD_ existiert.'.formater({
                THISMOD: this.toString().escapeKCode(),
                OTHERMOD: mod.toString().escapeKCode()
            }));
            return false;
        }
    }



    this.getPersistence().setNumber('activated', 1);
    this.F_OnActivated();
    ModuleManager.self.refreshHooks();
    return true;
};

Module.prototype.toString = function toString() {
    return this.constructor.name;
};


/**
 * Gibt die Sichtbarkeit des Moduls zurück.
 * @return {boolean}
 */
Module.prototype.isVisible = function isVisible() {
    if(typeof this.visible === "undefined")
        return true;

    return this.visible;
};