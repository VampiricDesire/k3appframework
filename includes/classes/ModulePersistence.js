/**
 *
 * @param {Module} module
 * @constructor
 */



function ModulePersistence(module) {
    /**
     *
     * @type {Module}
     */
    this.module = module;
    this.PERSISTENCE_KEYS = typeof module.PERSISTENCE_KEYS === 'undefined' ? {} : module.PERSISTENCE_KEYS;
}


/**
 *
 * @returns {String}
 */
ModulePersistence.prototype.getPrefix = function getPrefix() {
    return "m" + this.module.toString() + "_";
};


/**
 *
 * @param {String} str
 * @returns {String}
 */
ModulePersistence.prototype.getKey = function getKey(str) {
    if(this.PERSISTENCE_KEYS.indexOf(str) >= 0) return str;
    return this.getPrefix() + str;
};


/**
 *
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasString = function hasString(key) {
    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().hasString(shortKey);
};


/**
 *
 * @param {String} key
 * @param {String} defaultValue
 * @returns {String}
 */
ModulePersistence.prototype.getString = function getString(key, defaultValue) {
    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().getString(shortKey, defaultValue);
};

/**
 *
 * @param {String} key
 * @param {String} value
 */
ModulePersistence.prototype.setString = function setString(key, value) {
    let shortKey = this.getKey(key);
    KnuddelsServer.getPersistence().setString(shortKey, value);
};


/**
 *
 * @param {String} key
 */
ModulePersistence.prototype.deleteString = function deleteString(key) {
    let shortKey = this.getKey(key);
    KnuddelsServer.getPersistence().deleteString(shortKey);
};


/**
 *
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasObject = function hasObject(key) {
    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().hasObject(shortKey);
};


/**
 *
 * @param {String} key
 * @param {Object} defaultValue
 * @returns {Object}
 */
ModulePersistence.prototype.getObject = function getObject(key, defaultValue) {
    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().getObject(shortKey, defaultValue);
};

/**
 *
 * @param {String} key
 * @param {Object} value
 */
ModulePersistence.prototype.setObject = function setObject(key, value) {
    let shortKey = this.getKey(key);
    KnuddelsServer.getPersistence().setObject(shortKey, value);
};


/**
 *
 * @param {String} key
 */
ModulePersistence.prototype.deleteObject = function deleteObject(key) {
    let shortKey = this.getKey(key);
    KnuddelsServer.getPersistence().deleteObject(shortKey);
};


/**
 *
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasNumber = function hasNumber(key) {
    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().hasNumber(shortKey);
};


/**
 *
 * @param {String} key
 * @param {Number} defaultValue
 * @returns {Number}
 */
ModulePersistence.prototype.getNumber = function getNumber(key, defaultValue) {
    if (typeof defaultValue === 'undefined') {
        defaultValue = 0;
    }

    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().getNumber(shortKey, defaultValue);
};

/**
 *
 * @param {String} key
 * @param {Number} value
 */
ModulePersistence.prototype.setNumber = function setNumber(key, value) {
    let shortKey = this.getKey(key);
    KnuddelsServer.getPersistence().setNumber(shortKey, value);
};


/**
 *
 * @param {String} key
 * @param {Number} value
 * @returns {Number}
 */
ModulePersistence.prototype.addNumber = function addNumber(key, value) {
    let shortKey = this.getKey(key);
    return KnuddelsServer.getPersistence().addNumber(shortKey, value);
};

/**
 *
 * @param {String} key
 */
ModulePersistence.prototype.deleteNumber = function deleteNumber(key) {
    let shortKey = this.getKey(key);
    KnuddelsServer.getPersistence().deleteNumber(shortKey);
};


ModulePersistence.prototype.updateUserNumberKey = function updateUserNumberKey(oldkey, newkey, forceDelete) {

    let oshortKey = this.getKey(oldkey);
    let nshortKey = this.getKey(newkey);


    forceDelete = forceDelete || false;
    if (forceDelete) {
        UserPersistenceNumbers.deleteAll(this.getKey(nshortKey));
    }
    UserPersistenceNumbers.updateKey(oshortKey, nshortKey);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Number} defaultValue
 * @returns {Number}
 */
ModulePersistence.prototype.getUserNumber = function getUserNumber(user, key, defaultValue) {
    if (typeof defaultValue === 'undefined') {
        defaultValue = 0;
    }

    let shortKey = this.getKey(key);

    return user.getPersistence().getNumber(shortKey, defaultValue);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasUserNumber = function hasUserNumber(user, key) {

    let shortKey = this.getKey(key);

    return user.getPersistence().hasNumber(shortKey);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Number} value
 */
ModulePersistence.prototype.setUserNumber = function setUserNumber(user, key, value) {

    let shortKey = this.getKey(key);

    user.getPersistence().setNumber(shortKey, value);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Number} value
 * @returns {Number}
 */
ModulePersistence.prototype.addUserNumber = function addUserNumber(user, key, value) {

    let shortKey = this.getKey(key);

    return user.getPersistence().addNumber(shortKey, value);
};


/**
 *
 * @param {User} user
 * @param {String} key
 */
ModulePersistence.prototype.deleteUserNumber = function deleteUserNumber(user, key) {

    let shortKey = this.getKey(key);

    user.getPersistence().deleteNumber(shortKey);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Object} object
 */
ModulePersistence.prototype.setUserObject = function setUserObject(user, key, object) {

    let shortKey = this.getKey(key);
    user.getPersistence().setObject(shortKey, object);
};

/**
 *
 * @param {User} user
 * @param {String} key
 * @param {Object} defaultValue
 * @returns {Object}
 */
ModulePersistence.prototype.getUserObject = function getUserObject(user, key, defaultValue) {

    let shortKey = this.getKey(key);
    if (typeof defaultValue === 'undefined')
        return user.getPersistence().getObject(shortKey);

    return user.getPersistence().getObject(shortKey, defaultValue);
};


/**
 *
 * @param {User} user
 * @param {String} key
 * @returns {Boolean}
 */
ModulePersistence.prototype.hasUserObject = function hasUserNumber(user, key) {

    let shortKey = this.getKey(key);
    return user.getPersistence().hasObject(shortKey);
};


/**
 *
 * @param {User} user
 * @param {String} key
 */
ModulePersistence.prototype.deleteUserObject = function deleteUserObject(user, key) {

    let shortKey = this.getKey(key);
    user.getPersistence().deleteObject(shortKey);
};

ModulePersistence.prototype.hasUserString = function hasUserString(user, key) {

    let shortKey = this.getKey(key);
    return user.getPersistence().hasString(shortKey);
};

ModulePersistence.prototype.getUserString = function getUserString(user, key, defaultValue) {
    if (typeof defaultValue === 'undefined') {
        defaultValue = '';
    }
    let shortKey = this.getKey(key);
    return user.getPersistence().getString(shortKey, defaultValue);
};

ModulePersistence.prototype.setUserString = function getUserString(user, key, value) {

    let shortKey = this.getKey(key);

    return user.getPersistence().setString(shortKey, value);
};

ModulePersistence.prototype.deleteUserString = function deleteUserString(user, key) {

    let shortKey = this.getKey(key);

    user.getPersistence().deleteString(shortKey);
};


