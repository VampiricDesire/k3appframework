/**
 * Vererbt Child die Klasse von Parent
 * @param child
 * @param parent
 */
Object.extends = function(child, parent) {
    child.prototype = Object.create(parent.prototype);
    child.prototype.super = function () {
        parent.call(this);
    };
    child.prototype.constructor = child;
    child.createSingleTon = function() {
        if(typeof child.self === 'undefined') {
            child.self = new child();
        }
    };
};

Object.getRecursivePropertyNames = function (obj) {

    let keys = Object.getOwnPropertyNames(obj);
    let prototype = Object.getPrototypeOf(obj);
    if(prototype !== null) {
        keys = keys.concat(Object.getRecursivePropertyNames(prototype));
    }
    return keys;

};