if(!Date.prototype.hasOwnProperty("getMillisecondsOfDay")) {
    /**
     * Gibt die heutige vergangenen Millisekunden zurück
     * @returns {number}
     */
    Date.prototype.getMillisecondsOfDay = function () {
        var today_abs = new Date();
        today_abs.setHours(0);
        today_abs.setMinutes(0);
        today_abs.setSeconds(0);

        var ms = this.getTime() - today_abs.getTime();
        return ms;
    };
}

if(!Date.prototype.hasOwnProperty("getMillisecondsOfMonth")) {
    /**
     * Gibt die vergangenen Millisekunden dieses Monats zurück
     * @returns {number}
     */
    Date.prototype.getMillisecondsOfMonth = function () {
        var month_abs = new Date();
        month_abs.setDate(0);
        month_abs.setHours(0);
        month_abs.setMinutes(0);
        month_abs.setSeconds(0);

        var ms = this.getTime() - month_abs.getTime();
        return ms;
    };
}

if(!Date.prototype.hasOwnProperty("getMillisecondsOfYear")) {
    /**
     * Gibt die vergangenen Millisekunden dieses Jahres zurück
     * @returns {number}
     */
    Date.prototype.getMillisecondsOfYear = function () {
        var year_abs = new Date();
        year_abs.setMonth(0);
        year_abs.setDate(1);
        year_abs.setHours(0);
        year_abs.setMinutes(0);
        year_abs.setSeconds(0);

        var ms = this.getTime() - year_abs.getTime();
        return ms;
    };
}



if(typeof Date.getWeekStart !== 'function') {
    /**
     * Gibt ein Dateobjekt zurück, welches Montag 00:00:00 entspricht
     * @returns {Date}
     */
    Date.getWeekStart = function() {
        var now = new Date();
        var day = now.getDay() || 7; // Get current day number, converting Sun. to 7)
        if(day != 1) { //go back in time to monday
            now.setHours(-24 * (day-1));
        }
        now.setHours(0);
        now.setMinutes(0);
        now.setSeconds(0)

        return now;
    };

}

if(!Date.prototype.hasOwnProperty("getMillisecondsOfWeek")) {
    /**
     * Gibt die vergangenen Millisekunden dieser Woche zurück
     * @returns {number}
     */
    Date.prototype.getMillisecondsOfWeek = function () {
        var ms = this.getTime() - Date.getWeekStart();
        return ms;
    };
}


if(!Date.prototype.hasOwnProperty("toGermanString")) {
    /**
     * Wandelt die Zeit in einen Datestring um, der in Deutschland üblich ist
     * @returns {string}
     */
    Date.prototype.toGermanString = function () {

        var date = this.getDate();
        if(date < 10)
            date = "0" + date;

        var month = this.getMonth()+1;
        if(month < 10)
            month = "0" + month;

        var seconds = this.getSeconds();
        if(seconds < 10)
            seconds = "0" + seconds;

        var minutes = this.getMinutes();
        if(minutes < 10)
            minutes = "0" + minutes;

        var hours = this.getHours();
        if(hours < 10)
            hours = "0" + hours;


        return date + "." + month + "." + this.getFullYear() + " - " + hours + ":" + minutes + ":" + seconds;
    };
}

if(!Date.prototype.hasOwnProperty("getWeek")) {
    /**
     * Liefert die Kalenderwoche
     * @return {number}
     */
    Date.prototype.getWeek = function () {
        var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
    };
}

Date.getEaster = function getEaster(year) {
    var f = Math.floor,
        // Golden Number - 1
        G = year % 19,
        C = f(year / 100),
        // related to Epact
        H = (C - f(C / 4) - f((8 * C + 13)/25) + 19 * G + 15) % 30,
        // number of days from 21 March to the Paschal full moon
        I = H - f(H/28) * (1 - f(29/(H + 1)) * f((21-G)/11)),
        // weekday for the Paschal full moon
        J = (year + f(year / 4) + I + 2 - C + f(C / 4)) % 7,
        // number of days from 21 March to the Sunday on or before the Paschal full moon
        L = I - J,
        month = 3 + f((L + 40)/44),
        day = L + 28 - 31 * f(month / 4);

    return new Date(year, month-1, day);
};

Date.isEasterTime = function isEasterTime() {

    var now = new Date();
    var easterSunday = Date.getEaster(now.getFullYear());
    var startDate = new Date(easterSunday); startDate.setDate(startDate.getDate() - 4);
    var endDate = new Date(easterSunday); endDate.setDate(endDate.getDate() + 4);


    return (now.getTime() > startDate.getTime()) && (now.getTime() < endDate.getTime());
};

Date.isValentinesDayTime = function isValentinesDayTime() {
    var now = new Date();
    var valentinesDay = new Date(now.getFullYear(), 1, 14);
    var startDate = new Date(valentinesDay); startDate.setDate(startDate.getDate()-1);
    var endDate = new Date(valentinesDay); endDate.setDate(endDate.getDate() + 2);
    return (now.getTime() > startDate.getTime()) && (now.getTime() < endDate.getTime());
};


Date.getRosenmontag = function getRosenmontag() {
    var now = new Date();
    var easterSunday = Date.getEaster(now.getFullYear());
    var startDate = new Date(easterSunday); startDate.setDate(easterSunday.getDate() - 48);
    return startDate;
};

Date.isFaschingTime = function isFaschingTime() {
    var now = new Date();
    var rosenmontag = Date.getRosenmontag();

    var startDate = new Date(rosenmontag); startDate.setDate(startDate.getDate()-4);
    var endDate = new Date(rosenmontag); endDate.setDate(endDate.getDate() + 2);
    return (now.getTime() > startDate.getTime()) && (now.getTime() < endDate.getTime());
};

Date.isHalloweenTime = function isHalloweenTime() {
    var now = new Date();
    var halloweenDay = new Date(now.getFullYear(), 10, 31);
    var startDate = new Date(halloweenDay); startDate.setDate(startDate.getDate()-3);
    var endDate = new Date(halloweenDay); endDate.setDate(endDate.getDate() + 3);
    return (now.getTime() > startDate.getTime()) && (now.getTime() < endDate.getTime());
};

Date.getFirstAdvent = function (year) {
    if(typeof year === 'undefined') { year = (new Date()).getFullYear() }
    var d = new Date(new Date(year, 11, 24, 0, 0, 0, 0).getTime() - 3 * 7 * 24 * 60 * 60 * 1000);
    while (d.getDay() != 0) {
        d = new Date(d.getTime() - 24 * 60 * 60 * 1000);
    }

    return d;
};
Date.prototype.getFirstAdvent = function() {
    return Date.getFirstAdvent(this.getFullYear());
};

Date.getChristmasBeginning = function(year) {
    if(typeof year === 'undefined') { year = (new Date()).getFullYear() }
    let advent = Date.getFirstAdvent(year);
    let firstDec = new Date(year,11,1);
    return advent.getTime() < firstDec.getTime()?advent:firstDec;
};
Date.prototype.getChristmasBeginning = function() {
    return Date.getChristmasBeginning(this.getFullYear());
};

Date.prototype.isChristmasTime = function() {
    let beginning = this.getChristmasBeginning();
    let end = new Date(this.getFullYear(), 11,27);
    return this.getTime().between(beginning.getTime(), end.getTime());
};
Date.isChristmasTime = function() {
    return (new Date()).isChristmasTime();
};