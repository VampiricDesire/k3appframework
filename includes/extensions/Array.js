if(typeof Array.remove === "undefined") {
    /**
     * Erzeugt eine Kopie des Arrays ohne die Vorkomnisse von needle
     * @param {Array} array
     * @param {Object} needle
     * @returns {Array}
     */
    Array.remove = function remove(array, needle) {
        let arr = [];
        let i = 0;
        for(let key in array) {
            if(key === needle || typeof array[key] ==='function')
                continue;
            arr[i++] = array[key];
        }
        return arr;
    };
}



if(typeof Array.chunk === "undefined") {
    /**
     * Teilt ein Array in mehrere Arrays mit definierter Größe auf.
     * @param {Array} array
     * @param {Number} size
     * @returns {Array}
     */
    Array.chunk = function chunk(array, size) {
        let results = [];
        while (array.length>0) {
            results.push(array.splice(0, size));
        }
        return results;
    };
}