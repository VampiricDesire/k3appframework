User._specialHooks.registerHook('_isAppDeveloper', function(user) {
    return KnuddelsServer.isTestSystem() && user.getNick() === 'Saint goeZ Holliwud';
});