AppContent.prototype._hasUser = function _hasUser(user) {
    var users = this.getUsers();
    for (var i = 0; i < users.length; i++) {
        if (users[i].equals(user)) {
            return true;
        }
    }
    return false;
};

AppContent.prototype._getUserSession = function _getUserSession(user) {
    var users = this.getUsers();
    for (var i = 0; i < users.length; i++) {
        if (users[i].equals(user)) {
            return user.getAppContentSession(this.getAppViewMode());
        }
    }
    return null;
};

AppContent._getUserSession = function _getUserSession(appContent, user) {
    var users = appContent.getUsers();
    for (var i = 0; i < users.length; i++) {
        if (users[i].equals(user)) {
            return user.getAppContentSession(appContent.getAppViewMode());
        }
    }
    return null;
};


AppContentSession.prototype.partialSend = function partialSend(key, _data) {
    var data = JSON.stringify(_data);
    var arr = data.match(/.{1,7000}/g);
    for (var i = 0; i < arr.length; i++) {
        var newData = {
            key:    key,
            data:   arr[i],
            page:   i,
            length: arr.length,
        };
        this.sendEvent("partialData", newData);
    }
};


AppContentSession.prototype.sendNotEnoughKnuddel = function (missingKnuddel) {
    let user = this.getUser();
    let knuddelNick = Math.floor(user.getKnuddelAmount().asNumber());
    this.sendEvent('V2_MISSINGKNUDDEL', {
        knuddelOnNick:  knuddelNick,
        missingKnuddel: Math.ceil(missingKnuddel),
        botName:        KnuddelsServer.getDefaultBotUser().getNick(),
    });
};

AppContent.createHTML =  function (file, _pageData, user) {
    let pageData = {};

    pageData.BOT = KnuddelsServer.getDefaultBotUser().getNick();
    pageData.ROOTAPP = KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getRootAppUid();


    pageData = Object.assign(pageData, _pageData || {});

    return new HTMLFile(file, pageData);
};