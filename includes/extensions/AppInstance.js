AppInstance.prototype.isSelf = function() {
    return this.getAppInfo().getAppId() === KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getAppId();
};