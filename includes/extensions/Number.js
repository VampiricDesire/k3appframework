if(!Number.prototype.hasOwnProperty("number_format")) {
    Number.prototype.number_format = function(decimals, dec_point, thousands_sep) {

        var n = !isFinite(+this) ? 0 : +this,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            toFixedFix = function (n, prec) {
                // Fix for IE parseFloat(0.55).toFixed(0) = 0;
                var k = Math.pow(10, prec);
                return Math.round(n * k) / k;
            },
            s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    };
}


if(!Number.prototype.hasOwnProperty("between")) {
    Number.prototype.between = function between(min, max) {
        var myMin = Math.min(min, max);
        var myMax = Math.max(min, max);
        var me = +this;
        return (me >= myMin && me <= myMax);
    };
}


Number.prototype._format = function(forceDeci){
    var num = this;

    var fulls = Math.floor(num);
    var decis = Math.round((num-fulls)*100);
    if(decis === 0 && !forceDeci) {
        decis = '';
    }
    else if(decis < 10) {
        decis = '0'+decis;
    } else {
        decis = decis.toString();
    }



    fulls = fulls.toString();
    var tmp = [""];
    var j = 0;
    for(var i = fulls.length - 1; i >= 0; i--) {
        if(tmp[j].length === 3) {
            j++;
            tmp[j] = "";
        }
        tmp[j] = fulls[i] + tmp[j];
    }
    tmp.reverse();
    var str = tmp.join('.') + (decis.length>0?','+decis:'');
    if(str.startsWith('-.')) {
        str = '-' + str.substring(2);
    }
    return str;
};

Number.prototype.precisionRound = function (precision) {
    var factor = Math.pow(10, precision);
    return Math.round(this * factor) / factor;
};