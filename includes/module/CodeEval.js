if(typeof CodeEval === 'undefined') {
    function CodeEval() {
        this.super();
        this.register();
    }
    Object.extends(CodeEval, Module);
}

CodeEval.prototype.cmdEvalCode = function(user, params) {
    if(!user._isAppDeveloper()) {
        K3AF.debug(user + ' got no permission for CodeEval');
        return;
    }
    this.eval(user, params);
};

CodeEval.prototype.eval = function (user, code) {
    code = code.removeComments().replaceAll("\n", " ").trim();
    try {
        user.sendPrivateMessage(
            eval(code)
        );
    } catch(e) {
        user.sendPrivateMessage(e);
    }
};

CodeEval.prototype.isActivated = function() {
    return true;
};
CodeEval.prototype.deactivate = function() {
    return false;
};

CodeEval.createSingleTon();