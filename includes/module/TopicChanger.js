if(typeof TopicChanger === 'undefined') {
    function TopicChanger() {
        this.super();
        this.register();
    }
    Object.extends(TopicChanger, Module);
}


TopicChanger.prototype.cmdSetTopic = function(user, params, func) {
    if(!user._isChannelOwner())  return;
    KnuddelsServer.getChannel().getChannelConfiguration().getChannelInformation().setTopic(params.formater({

    }));
};
TopicChanger.prototype.cmdGetTopic = function(user, params, func) {
    if(!user._isChannelOwner())  return;
    user.sendPrivateMessage('°BB°_Dieser Channel hat folgendes Thema:°°°#°$TOPIC'.formater({
        TOPIC: KnuddelsServer.getChannel().getChannelConfiguration().getChannelInformation().getTopic().escapeKCode()
    }));
};


TopicChanger.createSingleTon();