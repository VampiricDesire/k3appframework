if(typeof ChannelAdmin === 'undefined') {
    function ChannelAdmin() {
        this.super();
        this.register();
    }
    Object.extends(ChannelAdmin, Module);
}

ChannelAdmin.prototype.activate = function(user) {
    user = user || null;
    if (user == null || user._isChannelOwner()) {
        return Module.prototype.activate.call(this);
    } else {
        user._sendPrivateMessage("Du darfst dieses Modul nicht aktivieren.");
        return false;
    }
};

ChannelAdmin.prototype.cmdChannelAdmin = function(user, params, func) {
    if (!user.isChannelOwner() && !user._isAppDeveloper()) {
        return;
    }

    let ind = params.indexOf(':');
    let action = "";
    if (ind === -1) {
        action = params.toLowerCase();
        params = "";
    } else {
        action = params.substr(0, ind).trim().toLowerCase();
        params = params.substr(ind+1).trim();
    }

    if (action === "add") {
        let tUser = KnuddelsServer.getUserByNickname(params);
        if (tUser == null) {
            user._sendPrivateMessage('Dieser Nutzer ist mir nicht bekannt.');
            return;
        }

        this.getPersistence().setUserNumber(tUser, 'Owner', 1);
        user._sendPrivateMessage("Ich habe $USER als ChannelAdmin eingetragen.".formater({
            USER: tUser.getProfileLink()
        }));
    } else if(action === 'remove'){
        let tUser = KnuddelsServer.getUserByNickname(params);
        if (tUser == null) {
            user._sendPrivateMessage('Dieser Nutzer ist mir unbekannt.');
            return;
        }

        this.getPersistence().deleteUserNumber(tUser, 'Owner');
        user._sendPrivateMessage("Ich habe $USER als ChannelAdmin entfernt.".formater({
            USER: tUser.getProfileLink()
        }));
    } else {
        let users = UserPersistenceNumbers.getSortedEntries(this.getPersistence().getKey('Owner'), { count: 100});
        let arr = [];
        users.forEach(function(entry) {
            let tUser = entry.getUser();
            arr.push('_°BB°$USER°r°_'.formater({
                USER: tUser.getProfileLink()
            }));
        });

        user._sendPrivateMessage('Folgende Nutzer sind als ChannelAdmins eingetragen:°#r°' + arr.join(', '));
    }
};

ChannelAdmin.prototype._isChannelOwnerHook = function(user) {
    if (!this.isActivated()) {
        return false;
    }

    return this.getPersistence().getUserNumber(user, 'Owner', 0) > 0;
};

ChannelAdmin.prototype._isChannelModeratorHook = function(user) {
    if (!this.isActivated()) {
        return false;
    }

    return this.getPersistence().getUserNumber(user, 'Owner', 0) > 0;
};

ChannelAdmin.prototype.deactivate = function(user) {
    user = user || null;
    if (user == null || user._isChannelOwner()) {
        return Module.prototype.deactivate.call(this);
    } else {
        user._sendPrivateMessage("Du darfst dieses Modul nicht deaktivieren.");
        return false;
    }
};

ChannelAdmin.createSingleTon();

User._specialHooks.registerHook('_isChannelOwner', ChannelAdmin.self._isChannelOwnerHook.bind(ChannelAdmin.self));
User._specialHooks.registerHook('_isChannelModerator', ChannelAdmin.self._isChannelModeratorHook.bind(ChannelAdmin.self));