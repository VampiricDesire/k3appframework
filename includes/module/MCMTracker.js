if(typeof MCMTracker === 'undefined') {
    function MCMTracker() {
        this.super();
        this.register();
    }

    Object.extends(MCMTracker, Module);
}

MCMTracker.prototype.cmdMCMTracker = function(user) {
    this.onUserJoined(user);
};

MCMTracker.prototype.buildMCM = function() {
    if (this.lastCheck > (Date.now() - 10000)) {
        return;
    }

    let allInstances = KnuddelsServer.getAppAccess().getOwnInstance().getAllInstances(true);
    let tmp_channel = [];

    allInstances.forEach(function(instance) {
        let uid = instance.getAppInfo().getAppUid();
        let channelName = instance.getChannelName();
        let cms = [];
        let i = 0;
        KnuddelsServer.getAppAccess().getOwnInstance().getOnlineUsers(instance, UserType.Human).forEach(function(user) {
            i++;
            if (user.isChannelModerator()) {
                cms.push(user);
            }
        }, this);

        tmp_channel.push({name: channelName, cms: cms, users: i});
    }, this);

    this.channels = tmp_channel;
    this.channels.sort(function(a,b) {
        let aname = a.name;
        let bname = b.name;
        if (aname === KnuddelsServer.getChannel().getRootChannelName()) {
            aname += ' 1';
        }

        if (bname === KnuddelsServer.getChannel().getRootChannelName()) {
            bname += ' 1';
        }

        aname = aname.replace(KnuddelsServer.getChannel().getRootChannelName(),'').trim();
        bname = bname.replace(KnuddelsServer.getChannel().getRootChannelName(),'').trim();
        return parseInt(aname) - parseInt(bname);
    });

    this.lastCheck = Date.now();
};

MCMTracker.prototype.onUserJoined = function(user) {
    if (user._isChannelModerator() || user._isAppManager()) {
        this.buildMCM();
        let msg = '_MCMTracker_';
        let onlineMCM = [];
        this.channels.forEach(function(c) {
            msg += '°#r°_°BB>$CHANNEL|/go "|/go +"<° ($CNT User)°r°_: '.formater({CNT: c.users, CHANNEL: c.name.escapeKCode()});
            let cmlinks = [];
            c.cms.forEach(function(cm) {
                onlineMCM.push(cm);
                cmlinks.push('_'+cm.getProfileLink()+'_°°');
            });

            msg += cmlinks.join(', ');
        });

        let allCMs = KnuddelsServer.getChannel().getChannelConfiguration().getChannelRights().getChannelModerators();
        let cmlinks = [];
        allCMs.forEach(function(cm) {
            let online = false;
            if (!cm.isOnline())
                return;
            onlineMCM.forEach(function(ocm) {
                if (cm.equals(ocm)) {
                    online = true;
                }
            });

            cmlinks.push(
                (online?'°BB°':'°RR°')+'_'+
                cm.getProfileLink() +
                '°°_ (' + cm.getClientType() + ')'
            );
        });

        msg += '°##r°_Online:_ ';
        msg += cmlinks.join(', ');
        msg += '°#BB°_In einem Channel      °RR°In keinem Channel_';

        user.sendPrivateMessage(msg);
    }
};

MCMTracker.createSingleTon();