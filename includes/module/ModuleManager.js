if(typeof ModuleManager === 'undefined') {
    function ModuleManager() {
        this.super();
        ModuleManager.self = this;
        this._modules = [];
        this._activeModules = [];
        this._hooks = {};
        this._commands = {};
        this.register();
    }
    Object.extends(ModuleManager, Module);
}




ModuleManager.stopHandle = false;


Module.prototype.stopHandle = function () {
    ModuleManager.stopHandle = true;
};


Module.prototype.register = function () {
    ModuleManager.self._modules.push(this);
};

ModuleManager.prototype.isActivated = function () {
    return true;
};
ModuleManager.prototype.deactivate = function () {
    return false;
};

/**
 *
 * @param {AppInstance} appInstance
 * @param {String} type
 * @param {Object} data
 */
ModuleManager.prototype.onAppEventReceived = function (appInstance, type, data) {
    if (type === 'K3AF_refreshHooks' && appInstance.isSelf()) {
        this.refreshHooks(false);
    }
};

ModuleManager.prototype.findModule = function(name) {
    let module = this._modules.filter(function(module) {
        return module.toString().toLowerCase() === name.toLowerCase().trim();
    });
    if(module.length === 0) return false;
    return module[0];
};

ModuleManager.prototype.cmdK3AFModules = function (user, params, funcname) {
    if(!user._isAppManager()) return;
    let firstInd = params.indexOf(':');
    let command = params.toLowerCase();
    if(firstInd !== -1) {
        command = params.substring(0, firstInd).toLowerCase();
        params = params.substring(firstInd+1);
    }

    if(command === 'activate') {
        let modulename = params;
        let module = this.findModule(modulename);
        if(module === null) {
            user.sendPrivateMessage('Modul wurde nicht gefunden.');
            this.sendAdminOverview(user);
            return;
        }
        module.activate(user); return;
    } else if(command === 'deactivate' ) {
        let modulename = params;
        let module = this.findModule(modulename);
        if(module === null) {
            user.sendPrivateMessage('Modul wurde nicht gefunden.');
            this.sendAdminOverview(user);
            return;
        }
        module.deactivate(user); return;
    } else {
        this.sendAdminOverview(user);
    }
};

ModuleManager.prototype.refreshHooks = function refreshHooks(allChannel, sendOverview) {
    if(typeof sendOverview === 'undefined') {
        sendOverview = true;
    }
    if (typeof allChannel === 'undefined') {
        allChannel = true;
    }
    if (allChannel) {
        KnuddelsServer.getAppAccess().getOwnInstance().getAllInstances(true).forEach(function (appInstance) {
            appInstance.sendAppEvent('K3AF_refreshHooks', KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getAppId());
        }, this);
        return;
    }


    K3AF.debug('Starting refreshHooks');
    this._activeModules = [];

    //find activemodules
    this._modules.forEach(function (module) {
        if (module.isActivated()) {
            this._activeModules.push(module);
        }
    }, this);

    this._activeModules.sort(function (a, b) {
        return a.priority - b.priority;
    });

    //hooks && comands for activemodules
    let hooks = {};
    let commands = {};
    this._activeModules.forEach(function (module) {
        let properties = Object.getRecursivePropertyNames(module);
        let moduleHooks = properties.filter(function (property) {
            return (property.startsWith('on') || property.startsWith('may'));
        });
        moduleHooks.forEach(function (hookname) {
            if (typeof hooks[hookname] === 'undefined') {
                hooks[hookname] = [];
            }
            hooks[hookname].push(module);
        });

        let moduleCommands = properties.filter(function (property) {
            return property.startsWith('cmd');
        });
        moduleCommands.forEach(function (command) {
            let cmd = command.substring(3).toLowerCase();
            commands[cmd] = {
                m: module,
                cmd: command,
            };
        });


    }, this);
    this._hooks = hooks;
    this._commands = commands;

    let appHooks = Object.keys(App).filter(function (property) {
        return (property.startsWith('on') || property.startsWith('may'));
    });
    appHooks.forEach(function (hookname) {
        delete App[hookname];
    });

    let createHook = (function createHook(hookname) {
        let affectedModules = this._hooks[hookname].map(function (module) {
            return module.toString();
        });
        K3AF.debug('Creating hook for ' + hookname + ' (Modules: ' + affectedModules.join(', ') + ')');
        return eval('App. ' + hookname + ' = function ' + hookname + '() { return ModuleManager.self.manageHook("' + hookname + '", arguments); };');
    }).bind(this);
    for (let hookname in this._hooks) {
        // noinspection JSUnfilteredForInLoop
        createHook(hookname);
    }
    App.chatCommands = {};
    for (let command in this._commands) {
        // noinspection JSUnfilteredForInLoop
        let cmd = this._commands[command];
        // noinspection JSUnfilteredForInLoop
        K3AF.debug('Creating /' + command + ' for ' + cmd.m.toString());
        App.chatCommands[command] = this.handleFunction;
    }
    K3AF.debug('Created Hooks: ' + Object.keys(this._hooks).join(', '));
    K3AF.debug('Created Commands: ' + Object.keys(App.chatCommands).join(', '));
    KnuddelsServer.refreshHooks();

    sendOverview && ModuleManager.self.sendAdminOverview();

};


ModuleManager.prototype.handleFunction = function () {
    K3AF.debug('manage function: ' + JSON.stringify(arguments));
    var _self = ModuleManager.self;
    var m = _self._commands[arguments[2].toLowerCase()];
    var cmd = m.cmd;
    var module = m.m;
    if (module.isLoggingFunctions && !arguments[0]._isAppDeveloper()) {
        KnuddelsServer.getDefaultLogger().warn(arguments[0] + ' nutzt /' + arguments[2] + ' ' + arguments[1]);
    }
    module[cmd].apply(module, arguments);


};


ModuleManager.prototype.onUserJoined = function(user) {
    return this.sendAdminOverview(user);
};

ModuleManager.prototype.sendAdminOverview = function (user) {
    if (typeof user === 'undefined') {
        KnuddelsServer.getChannel().getOnlineUsers(UserType.Human).forEach(function (user) {
            this.sendAdminOverview(user);
        }, this);
        return this;
    }
    if (!user._isAppManager()) {
        return this;
    }

    let activated = [];
    let regged = [];
    for (let i = 0; i < this._modules.length; i++) {
        let module = this._modules[i];
        let moduleName = module.toString().escapeKCode();
        if (!module.isVisible()) {
            if (!user._isAppDeveloper())
                continue;
            moduleName += '*'
        }

        if (module.isActivated()) {
            activated.push('_°BB°°>_h$MODULE|/tf-overridesb  /k3afmodules deactivate:"<°°r°§'.formater({
                MODULE: moduleName,
            }));
        } else {
            regged.push('°RR°°>_h$MODULE|/tf-overridesb /k3afmodules activate:"<°°r°§'.formater({
                MODULE: moduleName,
            }));
        }
    }

    let msg = "°#r°" +
        "°BB°_" + KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getAppName() + "_°r° läuft in der Version _" + KnuddelsServer.getAppAccess().getOwnInstance().getAppInfo().getAppVersion() + "_" +
        "°#r°_K3AF Version:_ " + K3AF.VERSION + " _Chatserver Version:_ " + KnuddelsServer.getChatServerInfo().getRevision() + "     _Appserver Version:_ " + KnuddelsServer.getAppServerInfo().getRevision() +
        "°#r°_Unaktivierte Module:_ " + regged.join(", ") +
        "°#r°_Aktivierte Module:_ " + activated.join(", ") +
        "°#r°_°>Channelranking|/usersatisfaction<° - °>Feedback|/channelfeedback<°_";

    user._sendPrivateMessage(msg);

};

ModuleManager.prototype.manageHook = function (hookname, args) {
    args = Array.slice(args);
    ModuleManager.stopHandle = false;
    K3AF.debug('manage Hook: ' + JSON.stringify(arguments));

    if (hookname === 'onBeforeKnuddelReceived') {
        let transfer = args[0];
        if (typeof this._hooks[hookname] !== "undefined") {
            for (let i = 0; i < this._hooks[hookname].length; i++) {
                let module = this._hooks[hookname][i];
                module[hookname].apply(module, args);
                if (transfer.isProcessed()) {
                    return;
                }
                if (ModuleManager.stopHandle) {
                    break;
                }
            }
        }
        transfer.accept();

    } else if (hookname === "mayShowPublicMessage" || hookname === "mayShowPublicActionMessage") {
        let allowed = true;
        if (typeof this._hooks[hookname] !== "undefined") {
            for (let i = 0; i < this._hooks[hookname].length; i++) {
                let module = this._hooks[hookname][i];
                allowed &= module[hookname].apply(module, args);
                if (ModuleManager.stopHandle) {
                    break;
                }
            }
        }

        // noinspection EqualityComparisonWithCoercionJS
        return allowed == 1;
    } else if (hookname === 'mayJoinChannel') {
        if (typeof this._hooks[hookname] !== "undefined") {
            for (let i = 0; i < this._hooks[hookname].length; i++) {
                let module = this._hooks[hookname][i];
                let ret = module[hookname].apply(module, args);
                if (typeof ret !== "undefined") {
                    return ret;
                }
                if (ModuleManager.stopHandle) {
                    break;
                }
            }
        }
        return ChannelJoinPermission.accepted();
    } else if (hookname.startsWith("on")) {

        if (typeof this._hooks[hookname] !== "undefined") {
            for (var i = 0; i < this._hooks[hookname].length; i++) {
                let module = this._hooks[hookname][i];
                module[hookname].apply(module, args);
                if (ModuleManager.stopHandle) {
                    break;
                }
            }
        }
    }


};



ModuleManager.prototype.onAppStart = function () {
    this._activeModules.forEach(function (module) {
        module.F_OnActivated();
    });
    this.refreshHooks(false, true);
};
ModuleManager.createSingleTon();
