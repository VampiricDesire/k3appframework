if (typeof AutoUpdate === 'undefined') {
    function AutoUpdate() {
        this.super();
        this.register();

        this.timer = null;
    }

    Object.extends(AutoUpdate, Module);
}


AutoUpdate.prototype.updateFiles = function () {
    let files = KnuddelsServer.getAppAccess().getOwnInstance().getRootInstance().updateAppFiles();
    files = files.filter(function (file) {
        return file.endsWith('.js');
    });
    files = files.filter(K3AF.FileSystem.EXCLUDE_WWW).filter(function (path) {
        return !path.startsWith('shared/') || path.startsWith(K3AF.BASEPATH);
    });

    files.forEach(function (file) {
        K3AF.FileSystem.execute(file);
    });
    if (files.length > 0) {
        K3AF.debug('UPDATE: ' + JSON.stringify(files));
        ModuleManager.self.refreshHooks();
    }
};

AutoUpdate.prototype.F_OnActivated = function () {
    if (this.timer !== null) {
        clearInterval(this.timer);
    }
    this.timer = setInterval(this.updateFiles.bind(this), KnuddelsServer.isTestSystem() ? 5000 : 60000);
};

AutoUpdate.prototype.F_OnDeactivated = function () {
    if (this.timer !== null) {
        clearInterval(this.timer);
    }
};


AutoUpdate.createSingleTon();